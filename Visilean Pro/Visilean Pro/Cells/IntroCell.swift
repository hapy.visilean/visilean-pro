//
//  IntroCell.swift
//  Visilean Pro
//
//  Created by Gopal on 21/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit

class IntroCell: UICollectionViewCell {
    
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblHeader: UILabel!
    @IBOutlet var imgIntroSlide: UIImageView!
}
