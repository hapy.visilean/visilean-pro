//
//  TableViewCell.swift
//  Visilean Pro
//
//  Created by Gopal on 14/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet var img_profile_pic: UIImageView!
    @IBOutlet var lbl_mobileno: UILabel!
    @IBOutlet var lbl_email: UILabel!
    @IBOutlet var lbl_lastname: UILabel!
    @IBOutlet var lbl_firstname: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
