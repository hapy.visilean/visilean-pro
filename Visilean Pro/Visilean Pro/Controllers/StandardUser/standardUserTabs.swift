//
//  standardUserTabs.swift
//  Visilean Pro
//
//  Created by hapy gohil on 17/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import UIKit

class standardUserTabs: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        //self.navigationController?.title = "Tabs Bar"
        
        //self.tabBar.frame.height = 100
        let selectedColor   = colors.PrimaryBlue
        let unselectedColor = UIColor(red: 16.0/255.0, green: 224.0/255.0, blue: 223.0/255.0, alpha: 1.0)

        let unselectedImages = ["tasks_icon_normal" , "schedular_icon_normal" , "notification_icon_normal"]
        let selectedImages = ["tasks_icon_active" , "schedular_icon_active" , "notification_icon_active"]
        let labelNames = ["Tasks","Scheduler", "Notification"]
        if let count = self.tabBar.items?.count {
            for i in 0...(count-1) {
                let imageNameForSelectedState   = selectedImages[i]
                let imageNameForUnselectedState = unselectedImages[i]

                self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                self.tabBar.items?[i].image = UIImage(named: imageNameForUnselectedState)?.withRenderingMode(.alwaysOriginal)
                
                if Device.IS_IPAD == true
                {
                    let str = labelNames[i]
                    let rightInset = str.count * 10
                    
                    print(self.tabBar.items?[i].accessibilityFrame)
                    if str == "Tasks"
                    {
                        self.tabBar.items?[i].imageInsets = UIEdgeInsets(top: 0, left: 70, bottom: 0, right: CGFloat(0))
                        
                    }
                    else
                    {
                        self.tabBar.items?[i].imageInsets = UIEdgeInsets(top: 0, left: CGFloat(rightInset), bottom: 0, right: 0)
                        
                    }
                }
                
            }
        }
        
        //self.tabBarItem.imageInsets =
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: unselectedColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: selectedColor], for: .selected)

        
        // Do any additional setup after loading the view.
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class CustomHeightTabBar : UITabBar {
    @IBInspectable var height: CGFloat = 0.0

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        var sizeOfTab = super.sizeThatFits(size)
        
        if Device.IS_IPAD == true
        {
            height = 90
        }
        
        
        if height > 0.0 {
            sizeOfTab.height = height
        }
        
        return sizeOfTab
    }
}
