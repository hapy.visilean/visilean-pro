//
//  LoginController.swift
//  Visilean Pro
//
//  Created by hapy gohil on 21/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit

class LoginController: UIViewController {

    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var btnLogin: GradientButton!
    @IBOutlet var lblErrorPassword: UILabel!
    @IBOutlet var lblErrorEmail: UILabel!
    @IBOutlet var btnShowPassword: GradientButton!
    @IBOutlet var ivCheckmark: UIImageView!
    var showPsasword = false
    var rememberSelected = false
    
    var loginVM = LoginVM()
    override func viewDidLoad() {
        super.viewDidLoad()

        txtEmail.delegate = self
        txtPassword.delegate = self

        lblErrorEmail.text = ""
        lblErrorPassword.text = ""
        loginVM.authenticateUserWith(email: "", password: "")
        self.checkAllValidations()
        btnLogin.makeDisable()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .default
       }
    @IBAction func btnClickRememberMe(_ sender: Any) {
        if rememberSelected == false
        {
            ivCheckmark.image = UIImage.init(named: "checked")
            rememberSelected = true
        }
        else
        {
            ivCheckmark.image = UIImage.init(named: "unchecked")
            rememberSelected = false
        }
    }
    
    @IBAction func btnClickShowPassword(_ sender: Any) {
        if showPsasword == false
        {
            txtPassword.isSecureTextEntry = false
            btnShowPassword.setImage(UIImage.init(named: "eye_visible_icon"), for: .normal)
            showPsasword = true
        }
        else
        {
            txtPassword.isSecureTextEntry = true
            btnShowPassword.setImage(UIImage.init(named: "eye_hide_icon"), for: .normal)
            showPsasword = false
        }
    }
    
    @IBAction func btnClickBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnClickLogin(_ sender: Any) {
        //let nextVC = UIStoryboard.RegisterStep1()
        //self.navigationController?.pushViewController(nextVC, animated: true)
        self.callLogin()
        //Application.Delegate.makeRootViewController()
        
    }
    
    func callLogin()
    {
        UserDefaults.standard.set(txtEmail.text!, forKey: "email")
        UserDefaults.standard.set(txtPassword.text!, forKey: "password")
        
        UserAuthentication.sharedInstance.userLoginAuthenticate()
    }
    
    @IBAction func btnClickForgotPassword(_ sender: Any) {
        let nextVC = UIStoryboard.ForgotPassword()
        //let nextVC = UIStoryboard.RegisterOTP()
        self.navigationController?.pushViewController(nextVC, animated: true)
    }

}

extension LoginController : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        btnLogin.makeDisable()
        
        if textField == txtEmail
        {
            loginVM.authenticateUserWith(email: updatedString ?? "", password: txtPassword.text ?? "")
        }
        
        if textField == txtPassword
        {
            loginVM.authenticateUserWith(email: txtEmail.text ?? "", password: updatedString ?? "")
        }
        
        self.checkAllValidations()
        
        return true
    }

    func checkAllValidations()
    {
        loginVM.loginComplitionHandler{ (status , loginMsg , passMsg) in
            if status == true
            {
                self.btnLogin.makeEnable()
            }
            else
            {
                self.btnLogin.makeDisable()
            }
            self.lblErrorEmail.text! = loginMsg
            self.lblErrorPassword.text = passMsg
        }
    }
}
