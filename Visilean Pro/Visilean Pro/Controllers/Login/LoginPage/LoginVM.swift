//
//  LoginVM.swift
//  Visilean Pro
//
//  Created by hapy gohil on 01/07/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import Foundation

class LoginVM {
    
    
    var emailError : String = ""
    var passwordError : String = ""
    
    typealias authenticationLoginCallback = (_ status : Bool , _ emailMsg : String , _ passwordMsg : String  ) -> Void
    
    var loginCallback : authenticationLoginCallback?
    
    func authenticateUserWith( email : String , password : String)
    {
        print(email)
        print(password)
        if checkEmail(str: email) == false
        {
            self.loginCallback?(false , emailError, passwordError)
        }
        
        if checkPassword(str: password) == false
        {
            self.loginCallback?(false , emailError, passwordError)
        }
        
        if emailError == "" && passwordError == ""
        {
            self.loginCallback?(true , emailError , passwordError)
        }
    }
    
    func loginComplitionHandler(callBack : @escaping authenticationLoginCallback)
    {
        self.loginCallback = callBack
    }
    
    func checkEmail(str : String) -> Bool
    {
        if str.isEmpty == true
        {
            emailError = "Please enter email"
            return false
        }
        else if str.isEmail == false
        {
            emailError = "Please enter valid email"
            return false
        }
        emailError = ""
        
        return true
    }
    
    func checkPassword(str : String) -> Bool
    {
        if str.isEmpty == true
        {
            passwordError = "Please enter password"
            return false
        }
        passwordError = ""
        return true
    }
}
