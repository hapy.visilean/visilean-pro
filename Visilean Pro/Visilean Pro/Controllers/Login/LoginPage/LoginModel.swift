//
//  LoginModel.swift
//  Visilean Pro
//
//  Created by hapy gohil on 16/06/20.
//  Copyright © 2020 Gopal. All rights reserved.
//


import Foundation
import SwiftyJSON


struct LoginResponseModel {

    var authorities = Array<JSON>()
    var accountNonExpired = 0
    var accountNonLocked = 0
    var credentialsNonExpired = 0
    var enabled = 0
    var password = ""
    var userId = ""
    var username = ""
    
    init(data: JSON) {
        self.authorities = data["authorities"].array!
        
        self.accountNonExpired = data["accountNonExpired"].int ?? 0
        self.accountNonLocked = data["accountNonLocked"].int ?? 0
        self.credentialsNonExpired = data["credentialsNonExpired"].int ?? 0
        self.enabled = data["enabled"].int ?? 0
        
        self.password = data["password"].string ?? ""
        self.userId = data["userId"].string ?? ""
        self.username = data["username"].string ?? ""
       
    }
}

struct authoritiesModel {

    var name = ""
    var roleScope = ""
    var uiName = ""
    
    
    init(data: JSON) {
        
        self.name = data["name"].string ?? ""
        self.roleScope = data["roleScope"].string  ?? ""
        self.uiName = data["uiName"].string ?? ""
        
    }
}


