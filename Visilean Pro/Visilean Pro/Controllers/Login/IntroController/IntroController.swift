//
//  IntroController.swift
//  Visilean Pro
//
//  Created by Gopal on 21/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit
import CHIPageControl

class IntroController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, CHIBasePageControlDelegate {

    @IBOutlet var introCollectionView: UICollectionView!
    internal let numberOfPages = 5
    @IBOutlet var pageControls: [CHIBasePageControl]!
    @IBOutlet var coloredPageControls: [CHIBasePageControl]!
    @IBOutlet weak var verticalPageControl: CHIPageControlJalapeno!
    
    @IBOutlet weak var CustomView: GradientView!
    @IBOutlet weak var lblOr: UILabel!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        introCollectionView.alpha = 0
        view1.alpha = 0
        view2.alpha = 0
        view3.alpha = 0
        lblOr.alpha = 0
        self.animShowCard()
        
    }
  func animShowCard(){
    UIView.animate(withDuration: 1.5, delay: 0, options: [.curveLinear],
                     animations: {
                        self.CustomView.center.y += self.CustomView.bounds.height
                        self.CustomView.layoutIfNeeded()

      },  completion: {(_ completed: Bool) -> Void in
               self.animShowAllViews()

          })
  }
   func animShowAllViews(){
    UIView.animate(withDuration: 1, delay: 0.5, options: [.curveEaseOut],
                        animations: {
                            self.introCollectionView.alpha = 1
                            self.view1.alpha = 1
                            self.view2.alpha = 1
                            self.view3.alpha = 1
                            self.lblOr.alpha = 1

         },  completion: {(_ completed: Bool) -> Void in
           
             })
     }
    func didTouch(pager: CHIBasePageControl, index: Int) {
           print(pager, index)
       }
    // MARK: - Collectionview Delegate
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            return 5

        }

        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCell", for: indexPath) as! IntroCell
            cell.imgIntroSlide.image = UIImage(named: "graphic")
            cell.lblHeader.text = "Digitalise Construction"
            cell.lblDescription.text = "Introducing an incredibly easy solution to transition in to the new digital era.Introducing an incredibly easy solution to transition in to the new digital era."

            return cell
        }
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

            
        }

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

       //    CGSize(width: introCollectionView.bounds.size.width, height: introCollectionView.bounds.size.height);
            let  height :CGFloat = introCollectionView.frame.size.height
            let width :CGFloat  = introCollectionView.frame.size.width
              // in case you you want the cell to be 40% of your controllers view
            return CGSize(width: width , height: height);
        }
       func scrollViewDidScroll(_ scrollView: UIScrollView) {
          let total = scrollView.contentSize.width - scrollView.bounds.width
          let offset = scrollView.contentOffset.x
          let percent = Double(offset / total)
          
          let progress = percent * Double(self.numberOfPages - 1)
          
          (self.pageControls).forEach { (control) in
              control.progress = progress
          }
      }
    
    // MARK: - Buttons Clicks
    
    @IBAction func btnClickSignUp(_ sender: Any) {
        let VC = UIStoryboard.RegisterStep1()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    @IBAction func btnClickLogin(_ sender: Any) {
        let VC = UIStoryboard.LoginPage()
        //let VC = UIStoryboard.ProjectList()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    @IBAction func btnClickSignInWithLinkedIn(_ sender: Any)
    {
    }
    @IBAction func btnClickSignInWithGoogle(_ sender: Any)
    {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//extension UIView{
//    func animShow(){
//        UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseIn],
//                       animations: {
//                        self.center.y -= self.bounds.height
//                        self.layoutIfNeeded()
//        }, completion: nil)
//        self.isHidden = false
//    }
//    func animHide(){
//        UIView.animate(withDuration: 2, delay: 0, options: [.curveLinear],
//                       animations: {
//                        self.center.y += self.bounds.height
//                        self.layoutIfNeeded()
//
//        },  completion: {(_ completed: Bool) -> Void in
//        self.isHidden = true
//            })
//    }
//}
