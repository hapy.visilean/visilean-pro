//
//  ForgotPasswordController.swift
//  Visilean Pro
//
//  Created by hapy gohil on 21/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit
import SwiftyJSON

class ForgotPasswordController: UIViewController , ShowsAlert, UIGestureRecognizerDelegate {

    @IBOutlet var lblErrorEmail: UILabel!
    @IBOutlet var txtEmail: UITextField!
    
    @IBOutlet var btnSubmit: GradientButton!
    
    var viewModel = forgotPasswordViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        txtEmail.delegate = self
        checkValidation(txtBox: "")
        btnSubmit.makeDisable()
        //btnSubmit.isFocused = false
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isEqual(navigationController?.interactivePopGestureRecognizer) {
            navigationController?.popViewController(animated: true)
        }
        return false
    }
        
    func bindData(){
        self.viewModel.setValidData = { (finished, error) in
            if !error {
                self.showAlert(title: Message.title, message: self.viewModel.forgotPassModel?.responseMessage ?? "")
            }
        }
    }
        
    @IBAction func btnClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnClickSubmit(_ sender: Any) {
        if checkValidation(txtBox: txtEmail.text!) == true
        {
            viewModel.configureDataModel(email: txtEmail.text!)
            bindData()
        }
    }
    
}

extension ForgotPasswordController : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        let validation = checkValidation(txtBox: updatedString ?? "")
        if validation == false
        {
            btnSubmit.makeDisable()
        }
        else
        {
            btnSubmit.makeEnable()
        }
        return true
    }
}

extension ForgotPasswordController
{
    func checkValidation(txtBox : String) -> Bool
    {
        if txtBox.isEmpty == true
        {
            lblErrorEmail.text = "Please enter email"
            return false
        }
        else if txtBox.isEmail == false
        {
            lblErrorEmail.text = "Please enter correct email-id"
            return false
        }
        else
        {
            lblErrorEmail.text = ""
            return true
        }
    }
}
