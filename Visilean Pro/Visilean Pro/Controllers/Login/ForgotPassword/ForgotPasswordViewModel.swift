//
//  ForgotPasswordViewModel.swift
//  Visilean Pro
//
//  Created by hapy gohil on 30/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ForgotPasswordModel {

    var responseMessage = ""
    var timestamp = 0.0
   
    init(data: JSON) {
        self.responseMessage = data["responseMessage"].string ?? ""
        self.timestamp = data["timestamp"].double ?? 0.0
    }
}


protocol forgotPasswordViewModelDelegate
{
    var setValidData : ((Bool , Bool) -> Void)? { get set }
}

class forgotPasswordViewModel: forgotPasswordViewModelDelegate , ShowsAlert{
    var setValidData: ((Bool, Bool) -> Void)?
    
    var dataModel : ResponseModelDictionary? = nil
    
    var forgotPassModel : ForgotPasswordModel? {
        didSet {
            self.setValidData!(true , false)
        }
        
    }
    
    
    func configureDataModel(email : String)
    {
        APIManager.sharedInstance.resetPassword(username: email){
            (response , error)  in
        
            if response != nil
            {
                self.dataModel = ResponseModelDictionary(data: response!)
                if self.dataModel!.status == 1
                {
                    self.configureOutput()
                    //self.showAlert(title: "Visilean Pro", message: fp.responseMessage)
                }
            }
            else
            {
                //self.showAlert(title: "Visilean Pro", message: error?.description ?? "Error in server")
            }
        }
    }
    
    func configureOutput() {
        
        self.forgotPassModel = ForgotPasswordModel(data: self.dataModel!.result)
        //print(self.loginModel!.ModifiedDate)
         
    }
    
}
