//
//  ListViewController.swift
//  Visilean Pro
//
//  Created by Gopal on 13/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit
import AlamofireImage

class ListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    private var listViewModel = ListViewModel()
    @IBOutlet var tblview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblview.dataSource = self
        tblview.delegate = self

        listViewModel.viewDidLoad()
        setUpUI()
    }
    private func setUpUI() {
           

           self.listViewModel.getListData = { (finished, error) in
               if !error {
                print(self.listViewModel.listModel?.count as Any)
                self.tblview.reloadData()
               }
           }
    }
    
    // MARK: - Table View

     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listViewModel.listModel?.count ?? 0
    }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
        let cell : TableViewCell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell") as! TableViewCell
       
        cell.lbl_firstname.text = listViewModel.listModel?[indexPath.row].FirstName
        cell.lbl_lastname.text = listViewModel.listModel?[indexPath.row].LastName
        cell.lbl_email.text = listViewModel.listModel?[indexPath.row].Emailid
        cell.lbl_mobileno.text = listViewModel.listModel?[indexPath.row].MobileNumber
        let url = URL(string: (listViewModel.listModel?[indexPath.row].ProfilePics)!)!
        cell.img_profile_pic.af.setImage(withURL: url)
        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
          //  tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
           return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return UITableView.automaticDimension
    }
 
    // MARK: - Button Click Events
       
    @IBAction func Btn_back_clicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
