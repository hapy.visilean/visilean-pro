//
//  ListModel.swift
//  Visilean Pro
//
//  Created by Gopal on 13/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ListResponseModel  {

    var ilist = Array<JSON>()
    var Status = ""
    var Message = ""
   
    
    init(data: JSON) {
        self.ilist = data["ilist"].array!
        self.Status = data["Status"].string!
        self.Message = data["Message"].string!
       
    }
}
struct ListModel {

    var ServiceProviderId = 0
    var Subcatid = 0
    var IdentityProof = ""
    var Subcategoryname = ""
    var FirstName = ""
    var Ceritificate2 = "";
    var Password = ""
    var ProfilePics = ""
    var Approved = ""
    var CreateDate = ""
    var ProviderName = "";
    var Certificate = ""
    var AverageRating = 0
    var ModifiedDate = "";
    var City = "";
    var Emailid = "";
    var VendorDescription = "";
    var PasswordResetCode = "";
    var ServiceCharge = 0;
    var CategoryID = 0;
    var Categoryname = "";
    var Address = "";
    var LastName = "";
    var IsModified = "";
    var TotalRating = 0;
    var Experience = 0;
    var Team = 0;
    var MobileNumber = "";
    var ActivationCode = 0;
    var Status = "";

    
    init(data: JSON) {
        
        self.ServiceProviderId = data["ServiceProviderId"].int ?? 0
        self.Subcatid = data["Subcatid"].int ?? 0
        self.IdentityProof = data["IdentityProof"].string ?? ""
        self.Subcategoryname = data["Subcategoryname"].string ?? ""
        self.FirstName = data["FirstName"].string ?? ""
        self.Ceritificate2 = data["Ceritificate2"].string ?? ""
        self.Password = data["Password"].string ?? ""
        self.ProfilePics = data["ProfilePics"].string ?? ""
        self.Approved = data["Approved"].string ?? ""
        self.CreateDate = data["CreateDate"].string ?? ""
        self.ProviderName = data["ProviderName"].string ?? ""
        self.Certificate = data["Certificate"].string ?? ""
        self.AverageRating = data["AverageRating"].int ?? 0
        self.ModifiedDate = data["ModifiedDate"].string ?? ""
        self.ModifiedDate = data["ModifiedDate"].string ?? ""
        self.City = data["City"].string ?? ""
        self.Emailid = data["Emailid"].string ?? ""
        self.VendorDescription = data["VendorDescription"].string ?? ""
        self.PasswordResetCode = data["PasswordResetCode"].string ?? ""
        self.CategoryID = data["CategoryID"].int ?? 0
        self.Categoryname = data["Categoryname"].string ?? ""
        self.Address = data["Address"].string ?? ""
        self.LastName = data["LastName"].string ?? ""
        self.IsModified = data["IsModified"].string ?? ""
        self.TotalRating = data["TotalRating"].int ?? 0
        self.Experience = data["Experience"].int ?? 0
        self.Team = data["Team"].int ?? 0
        self.MobileNumber = data["MobileNumber"].string ?? ""
        self.ActivationCode = data["ActivationCode"].int ?? 0
        self.Status = data["Status"].string ?? ""

    }
}
