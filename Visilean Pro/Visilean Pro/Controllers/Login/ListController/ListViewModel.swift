//
//  ListViewModel.swift
//  Visilean Pro
//
//  Created by Gopal on 13/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ListViewModelProtocol {

var getListData: ((Bool, Bool) -> Void)? { get set }
}

class ListViewModel : ListViewModelProtocol
{
var getListData: ((Bool, Bool) -> Void)?

    private var dataModel: ListResponseModel? = nil

var listModel: [ListModel]? {
    didSet {
        self.getListData!(true, false)
    }
}


 func viewDidLoad() {

    configureListModel()
}

private func configureListModel() {
      
    APIManager.sharedInstance.requestList(){
        (response,error) in
    
        if error != nil
        {
            print(" \nResponse: \(response)")
            self.dataModel = ListResponseModel(data: response!)
            if self.dataModel!.Status == "1"
            {
                self.configureOutput()
            }
            else
            {
                self.configureError()
            }
        }
        
        
    }
     
}
  
   //Configure the output properties that are to be accessed by the view.
private func configureOutput() {
    
    var dataobj : [ListModel] = []
  
    for i in 0 ..< dataModel!.ilist.count
    {
        let dict = ListModel(data: dataModel!.ilist[i])
        print(dict)
        dataobj.append(dict)
    }
    
    self.listModel = dataobj
    //print(self.loginModel!.ModifiedDate)
     
}


private func configureError()
{
    print(dataModel!.Message)
}

}
