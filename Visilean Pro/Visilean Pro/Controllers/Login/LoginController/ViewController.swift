//
//  ViewController.swift
//  Visilean Pro
//
//  Created by hapy gohil on 11/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController {

    private var viewModel = ViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
     
        viewModel.viewDidLoad()
        setUpUI()
        
    }
    private func setUpUI() {
        

        self.viewModel.getValidLoginData = { (finished, error) in
            if !error {
                print(self.viewModel.loginModel?.FirstName as Any)
                print(self.viewModel.loginModel?.LastName as Any)
                print(self.viewModel.loginModel?.Emailid as Any)
                
            }
        }

        
    }
    
    
    // MARK: - Button Click Events
    
    @IBAction func Btn_Login_Clicked(_ sender: Any)
    {
        let ListVC = UIStoryboard.ListViewControllerScreen()
        self.navigationController?.pushViewController(ListVC, animated: true)
        
    }
    
    
    /*
       // MARK: - Navigation

       // In a storyboard-based application, you will often want to do a little preparation before navigation
       override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           // Get the new view controller using segue.destination.
           // Pass the selected object to the new view controller.
       }
       */

}

