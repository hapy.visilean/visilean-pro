//
//  Model.swift
//  Demo VisileanPRO
//
//  Created by Gopal on 10/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import Foundation
import SwiftyJSON



struct ResponseModelJsonArray {

    var result = Array<JSON>()
    var status = 0
    var message = ""
   
    
    init(data: JSON) {
        self.result = data["result"].array!
        self.status = data["status"].int!
        self.message = data["message"].string!
       
    }
}


struct ResponseModelStringsArray {

    var result = Array<String>()
    var status = 0
    var message = ""
   
    
    init(data: JSON) {
        self.result = data["result"].arrayObject! as! [String]
        self.status = data["status"].int!
        self.message = data["message"].string!
       
    }
}

struct ResponseModelDictionary {

    var result = JSON()
    var status = 0
    var message = ""
    var error = Array<String>()
    
    init(data: JSON) {
        self.result = data["result"]
        self.status = data["status"].int!
        self.message = data["message"].string!
        self.error = data["errors"].arrayObject as? [String] ?? []
    }
}

struct LoginModel {

    var ActivationCode = ""
    var Address = ""
    var CreateDate = ""
    var Emailid = ""
    var FirstName = ""
    var IsSocial = 0;
    var LastName = ""
    var MobileNumber = ""
    var ModifiedDate = ""
    var Password = ""
    var RegId = 0;
    var ResetPasswordCode = ""
    var Socialid = ""
    var Status = 0;
    
    
    init(data: JSON) {
        
        self.ActivationCode = data["ActivationCode"].string ?? ""
        self.Address = data["Address"].string  ?? ""
        self.CreateDate = data["CreateDate"].string ?? ""
        self.Emailid = data["Emailid"].string ?? ""
        self.FirstName = data["FirstName"].string ?? ""
        self.IsSocial = data["IsSocial"].int ?? 0
        self.LastName = data["LastName"].string ?? ""
        self.MobileNumber = data["MobileNumber"].string ?? ""
        self.ModifiedDate = data["ModifiedDate"].string ?? ""
        self.Password = data["Password"].string ?? ""
        self.RegId = data["RegId"].int ?? 0
        self.ResetPasswordCode = data["ResetPasswordCode"].string ?? ""
        self.Socialid = data["Socialid"].string ?? ""
        self.Status = data["Status"].int ?? 0
        
        
    }
}

