//
//  ViewModel.swift
//  Demo VisileanPRO
//
//  Created by Gopal on 10/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol ViewModelProtocol {
    
    var getValidLoginData: ((Bool, Bool) -> Void)? { get set }
    
}


class ViewModel : ViewModelProtocol{
    var getValidLoginData: ((Bool, Bool) -> Void)?
    
    private var dataModel: ResponseModelJsonArray! = nil
    
    var loginModel: LoginModel? {
        didSet {
            self.getValidLoginData!(true, false)
        }
    }
    
    
     func viewDidLoad() {

        configureDataModel()
    }
    
    private func configureDataModel() {
          
        APIManager.sharedInstance.userLogin(username: "test1@gmail.com", password: "test1"){
            (response,error) in
        
            print(" \nResponse: \(response)")
            self.dataModel = ResponseModelJsonArray(data: response!)
            if self.dataModel.status == 1
            {
                print(self.dataModel.message)
                toast(self.dataModel.message, size: .small, duration: .normal)

                self.configureOutput()
            }
            else
            {
                self.configureError()
            }
            
        }
         
    }
      
       //Configure the output properties that are to be accessed by the view.
    private func configureOutput() {
        
        self.loginModel = LoginModel(data: dataModel.result[0])
        
        //print(self.loginModel!.ModifiedDate)
         
    }
    
    
    private func configureError()
    {
        print(dataModel.message)
    }
    

}
