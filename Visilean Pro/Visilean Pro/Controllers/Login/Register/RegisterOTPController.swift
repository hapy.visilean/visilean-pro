//
//  RegisterOTPController.swift
//  Visilean Pro
//
//  Created by hapy gohil on 21/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit

class RegisterOTPController: UIViewController , UITextFieldDelegate, UIGestureRecognizerDelegate{

    @IBOutlet var lblMobile: UILabel!
    @IBOutlet var txtBox4: UITextField!
    @IBOutlet var txtBox3: UITextField!
    @IBOutlet var txtBox2: UITextField!
    @IBOutlet var txtBox1: UITextField!
    
    @IBOutlet var btnVerify: GradientButton!
    @IBOutlet var btnResend: GradientButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        txtBox1.delegate = self
        txtBox2.delegate = self
        txtBox3.delegate = self
        txtBox4.delegate = self
        
        setActiveTextField(text: txtBox1)
        
        txtBox1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        txtBox2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        txtBox3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        txtBox4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        
        btnVerify.makeDisable()
        self.setActiveTextField(text: txtBox1)
        self.setInactiveTextField(text: txtBox2)
        self.setInactiveTextField(text: txtBox3)
        self.setInactiveTextField(text: txtBox4)

        //self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isEqual(navigationController?.interactivePopGestureRecognizer) {
            navigationController?.popViewController(animated: true)
        }
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .default
       }
    @objc func textFieldDidChange(textField: UITextField)
    {
        if checkAllValidation() == true
        {
            btnVerify.makeEnable()
        }
        else
        {
            btnVerify.makeDisable()
        }
        
        let text = textField.text
        if text?.utf16.count == 1 {
            switch textField {
            case txtBox1:
                setActiveTextField(text: txtBox1)
                txtBox2.becomeFirstResponder()
            case txtBox2:
                setActiveTextField(text: txtBox2)
                txtBox3.becomeFirstResponder()
            case txtBox3:
                setActiveTextField(text: txtBox3)
                txtBox4.becomeFirstResponder()
            case txtBox4:
                setActiveTextField(text: txtBox4)
                txtBox4.resignFirstResponder()
            default:
                break
            }
        } else {
            switch textField {
            case txtBox4:
                setInactiveTextField(text: txtBox4)
                txtBox3.becomeFirstResponder()
            case txtBox3:
                setInactiveTextField(text: txtBox3)
                txtBox2.becomeFirstResponder()
            case txtBox2:
                setInactiveTextField(text: txtBox2)
                txtBox1.becomeFirstResponder()
            case txtBox1:
                setInactiveTextField(text: txtBox1)
                txtBox1.resignFirstResponder()
            default:
                break
            }
        }
    }

    
    @IBAction func btnClickResend(_ sender: Any) {
    }
    
    @IBAction func btnClickVerify(_ sender: Any) {
        let nextVC = UIStoryboard.RegisterStep2()
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    
    func setInactiveTextField(text : UITextField)
    {
        text.makeRoundedCornerTextField(Radius: 10, borderColor: UIColor.lightGray, padding: 0)
    }
    
    func setActiveTextField(text : UITextField)
    {
        text.makeRoundedCornerTextField(Radius: 10, borderColor: colors.PrimaryBlue , padding: 0
        )
    }

    func checkAllValidation() -> Bool
    {
        if txtBox1.text?.isEmpty == true
        {
            return false
        }
        else if txtBox2.text?.isEmpty == true
        {
            return false
        }
        else if txtBox3.text?.isEmpty == true
        {
            return false
        }
        else if txtBox4.text?.isEmpty == true
        {
            return false
        }
        else
        {
            return true
        }
    }
    
    @IBAction func btnClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
