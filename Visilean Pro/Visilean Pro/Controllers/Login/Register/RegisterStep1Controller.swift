//
//  RegisterStep1Controller.swift
//  Visilean Pro
//
//  Created by hapy gohil on 21/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit
import FlagPhoneNumber
import SwiftyJSON

class RegisterStep1Controller: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet var btnNext: GradientButton!
    
    @IBOutlet var lblErrorPassword: UILabel!
    @IBOutlet var lblErrorMobile: UILabel!
    @IBOutlet var lblErrorEmail: UILabel!
    @IBOutlet var lblErrorFirstname: UILabel!
    
    @IBOutlet var txtMobile: FPNTextField!
    @IBOutlet var txtPassword: UITextField!
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtFullname: UITextField!
    
    @IBOutlet var lblStep: UILabel!
    @IBOutlet var ivProfilePic: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setStepLabel()
        txtEmail.delegate = self
        txtMobile.delegate = self
        txtFullname.delegate = self
        txtPassword.delegate = self
        
        ivProfilePic.makeRoundedCornerView(Radius: 40)
//        self.checkEmail(str: "")
//        self.checkMobile(str: "", lbl: lblErrorMobile)
//        self.checkempty(str: "", lbl: lblErrorFirstname)
//        self.checkempty(str: "", lbl: lblErrorPassword)
        btnNext.makeDisable()
        
        self.getTimezones()
        self.setCountry()
        setNeedsStatusBarAppearanceUpdate()

        
        //self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isEqual(navigationController?.interactivePopGestureRecognizer) {
            navigationController?.popViewController(animated: true)
        }
        return false
    }

    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func clearData()
    {
        lblErrorEmail.text = ""
        lblErrorMobile.text = ""
        lblErrorPassword.text = ""
        lblErrorFirstname.text = ""
    }
    
    func getTimezones()
    {
        IndicatorView.sharedInstance.showIndicator()
        APIManager.sharedInstance.getCountryList(){
            (response , error) in
        
            IndicatorView.sharedInstance.hideIndicator()
            if response != nil
            {
                print(" \nResponse: \(response)")
                let model = ResponseModelStringsArray(data: response!)
                if model.status == 1
                {
                    print(model.result)
                }
            }
            else
            {
                print(" \nResponse: \(error?.description)")
            }
            
        }

    }
    
    @IBAction func btnClickNext(_ sender: Any) {
        let nextVC = UIStoryboard.RegisterStep2()
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    @IBAction func btnClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setStepLabel()
    {
        let myString:String = "Step 1 of 3"
        var myMutableString = NSMutableAttributedString()
        
        myMutableString = NSMutableAttributedString(string: myString, attributes: [NSAttributedString.Key.font :UIFont(name: "Poppins-Bold", size: 20.0)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:6,length:5))

        // set label Attribute
        lblStep.attributedText = myMutableString
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnClickUploadPhoto(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.delegate = self
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {
            action in
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: {
            action in
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)

    }
    
    func setCountry()
    {
        //txtMobile.borderStyle = .roundedRect
        //        phoneNumberTextField.pickerView.showPhoneNumbers = false
        txtMobile.displayMode = .list // .picker by default

        listController.setup(repository: txtMobile.countryRepository)

        listController.didSelect = { [weak self] country in
                    self?.txtMobile.setFlag(countryCode: country.code)   }

        txtMobile.delegate = self
        txtMobile.font = UIFont().setFontPoppins(size: 14, fontType: .Regular)

                // Custom the size/edgeInsets of the flag button
        txtMobile.flagButtonSize = CGSize(width: 35, height: 35)
        txtMobile.flagButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)

                // Example of customizing the textField input accessory view
        let items = [
                    UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.save, target: self, action: nil),
                    UIBarButtonItem(title: "Item 1", style: .plain, target: self, action: nil),
                    UIBarButtonItem(title: "Item 2", style: .plain, target: self, action: nil)
        ]
                
        txtMobile.textFieldInputAccessoryView = getCustomTextFieldInputAccessoryView(with: items)

        txtMobile.hasPhoneNumberExample = false
        txtMobile.placeholder = "Phone Number"
    }
    
    private func getCustomTextFieldInputAccessoryView(with items: [UIBarButtonItem]) -> UIToolbar {
            let toolbar: UIToolbar = UIToolbar()

            toolbar.barStyle = UIBarStyle.default
            toolbar.items = items
            toolbar.sizeToFit()

            return toolbar
    }

    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
    }
}

extension RegisterStep1Controller: FPNTextFieldDelegate {

    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
            //textField.rightViewMode = .always
            //textField.rightView = UIImageView(image: isValid ? #imageLiteral(resourceName: "success") : #imageLiteral(resourceName: "error"))

        print(
            isValid,
            textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
            textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
            textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
            textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
            textField.getRawPhoneNumber() ?? "Raw: nil"
        )
    }

    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code)
    }


    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)

        listController.title = "Countries"
        listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))

        self.present(navigationViewController, animated: true, completion: nil)
    }
}




extension RegisterStep1Controller : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        //let curretnValidation = checkValidation(txtBox: updatedString ?? "" , txtField: textField)
        //let allValidation = checkAllValiation()
        
        btnNext.makeDisable()
        
        if textField == txtEmail && self.checkEmail(str: updatedString ?? "") == true
        {
            btnNext.makeEnable()
        }
        
        if textField == txtFullname && self.checkempty(str: updatedString ?? "", lbl: lblErrorFirstname) == true
        {
            btnNext.makeEnable()
        }
        
        if textField == txtMobile && self.checkMobile(str: updatedString ?? "", lbl: lblErrorMobile) == true
        {
            btnNext.makeEnable()
        }
        
        if textField == txtPassword && self.checkempty(str: updatedString ?? "", lbl: lblErrorPassword) == true
        {
            btnNext.makeEnable()
        }
        
        if checkAllValiation() == true
        {
            btnNext.makeEnable()
        }
        else
        {
            btnNext.makeDisable()
        }
        
        return true
    }

}

extension RegisterStep1Controller{
    
    
    func checkAllValiation() -> Bool
    {
        if lblErrorEmail.text?.isEmpty == true && lblErrorPassword.text?.isEmpty == true && lblErrorMobile.text?.isEmpty == true && lblErrorFirstname.text?.isEmpty == true
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func checkEmail(str : String) -> Bool
    {
        if str.isEmpty == true
        {
            lblErrorEmail.text = "Please enter email"
            return false
        }
        else if str.isEmail == false
        {
            lblErrorEmail.text = "Please enter valid email"
            return false
        }
        lblErrorEmail.text = ""
        
        return true
    }
    
    func checkMobile(str : String , lbl : UILabel) -> Bool
    {
        if str.isEmpty == true
        {
            lbl.text = "Please enter mobile number"
            return false
        }
        else if str.isValidPhone == false
        {
            lbl.text = "Please enter valid mobile number"
            return false
        }
        lbl.text = ""
        
        return true
    }
    
    func checkempty(str : String , lbl : UILabel) -> Bool
    {
        if str.isEmpty == true
        {
            if lbl == lblErrorFirstname
            {
                lbl.text = "Please enter firstname"
            }
            else if lbl == lblErrorPassword
            {
                lbl.text = "Please enter password"
            }
            
            return false
        }
        
        lbl.text = ""
        
        return true
    }

}

extension RegisterStep1Controller : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    // Image Picker Delegate
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    // Local variable inserted by Swift 4.2 migrator.
            let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

            let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
            ivProfilePic.image = image
            picker.dismiss(animated: true, completion: nil)
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
        
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
