//
//  RegisterStep3Controller.swift
//  Visilean Pro
//
//  Created by hapy gohil on 21/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit

class RegisterStep3Controller: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet var lblErrorLocation: UILabel!
    @IBOutlet var lblErrorProjectName: UILabel!
    @IBOutlet var lblStep: UILabel!
    @IBOutlet var btnNext: GradientButton!
    @IBOutlet var txtVDesc: UITextView!
    @IBOutlet var txtLocation: UITextField!
    @IBOutlet var ivTearms: UIImageView!
    @IBOutlet var ivSampleProjectTick: UIImageView!
    @IBOutlet var txtProjectName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStepLabel()

        txtLocation.delegate = self
        txtProjectName.delegate = self
        var result = false
        result = self.checkempty(str: "", lbl: lblErrorLocation)
        result = self.checkempty(str: "", lbl: lblErrorProjectName)
        print(result)
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isEqual(navigationController?.interactivePopGestureRecognizer) {
                navigationController?.popViewController(animated: true)
        }
        return false
    }
        
    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .default
       }
    @IBAction func btnClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnClickSampleProject(_ sender: Any) {
    }
    @IBAction func btnClickTermsConditions(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func setStepLabel()
    {
        let myString:String = "Step 3 of 3"
        var myMutableString = NSMutableAttributedString()
        
        myMutableString = NSMutableAttributedString(string: myString, attributes: [NSAttributedString.Key.font :UIFont(name: "Poppins-Bold", size: 20.0)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:6,length:5))

        // set label Attribute
        lblStep.attributedText = myMutableString
    }
    @IBAction func btnClickNext(_ sender: Any) {
    }
}



extension RegisterStep3Controller : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        //let curretnValidation = checkValidation(txtBox: updatedString ?? "" , txtField: textField)
        //let allValidation = checkAllValiation()
        
        btnNext.makeDisable()
        
        
        if textField == txtProjectName && self.checkempty(str: updatedString ?? "", lbl: lblErrorProjectName)
        {
            btnNext.makeEnable()
        }
        
        if textField == txtLocation && self.checkempty(str: updatedString ?? "", lbl: lblErrorLocation)
        {
            btnNext.makeEnable()
        }
        
        
        
        if checkAllValiation() == true
        {
            btnNext.makeEnable()
        }
        else
        {
            btnNext.makeDisable()
        }
        
        return true
    }

}


extension RegisterStep3Controller{
    
    
    func checkAllValiation() -> Bool
    {
        if lblErrorProjectName.text?.isEmpty == true && lblErrorLocation.text?.isEmpty == true
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    
    func checkempty(str : String , lbl : UILabel) -> Bool
    {
        if str.isEmpty == true
        {
            if lbl == lblErrorProjectName
            {
                lbl.text = "Please enter project name"
            }
            else if lbl == lblErrorLocation
            {
                lbl.text = "Please add location"
            }
            
            return false
        }
        
        lbl.text = ""
        
        return true
    }

}

