//
//  RegisterStep2Controller.swift
//  Visilean Pro
//
//  Created by hapy gohil on 21/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit

class RegisterStep2Controller: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet var txtRole: UITextField!
    @IBOutlet var btnNext: GradientButton!
    
    @IBOutlet var lblStep: UILabel!
    @IBOutlet var txtCompanyName: UITextField!
    @IBOutlet var lblErrorRole: UILabel!
    @IBOutlet var lblErrorComapny: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setStepLabel()
        
        txtRole.delegate = self
        txtCompanyName.delegate = self
        
        self.checkempty(str: "", lbl: lblErrorRole)
        self.checkempty(str: "", lbl: lblErrorComapny)
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isEqual(navigationController?.interactivePopGestureRecognizer) {
            navigationController?.popViewController(animated: true)
        }
        return false
    }
        
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .default
       }
    @IBAction func btnClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnClickNext(_ sender: Any) {
        let nextVC = UIStoryboard.RegisterStep3()
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func setStepLabel()
    {
        let myString:String = "Step 2 of 3"
        var myMutableString = NSMutableAttributedString()
        
        myMutableString = NSMutableAttributedString(string: myString, attributes: [NSAttributedString.Key.font :UIFont(name: "Poppins-Bold", size: 20.0)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:6,length:5))

        // set label Attribute
        lblStep.attributedText = myMutableString
    }
}


extension RegisterStep2Controller : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        //let curretnValidation = checkValidation(txtBox: updatedString ?? "" , txtField: textField)
        //let allValidation = checkAllValiation()
        
        btnNext.makeDisable()
        
        if textField == txtCompanyName && self.checkempty(str: updatedString ?? "", lbl: lblErrorComapny)
        {
            btnNext.makeEnable()
        }
        
        if textField == txtRole && self.checkempty(str: updatedString ?? "", lbl: lblErrorRole)
        {
            btnNext.makeEnable()
        }
        
        if checkAllValiation() == true
        {
            btnNext.makeEnable()
        }
        else
        {
            btnNext.makeDisable()
        }
        
        return true
    }

}


extension RegisterStep2Controller{
    func checkAllValiation() -> Bool
    {
        if lblErrorRole.text?.isEmpty == true && lblErrorComapny.text?.isEmpty == true
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    
    func checkempty(str : String , lbl : UILabel) -> Bool
    {
        if str.isEmpty == true
        {
            if lbl == lblErrorRole
            {
                lbl.text = "Please select any role"
            }
            else if lbl == lblErrorComapny
            {
                lbl.text = "Please add company name"
            }
            
            return false
        }
        
        lbl.text = ""
        
        return true
    }

}
