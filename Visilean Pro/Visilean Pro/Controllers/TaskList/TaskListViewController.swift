//
//  TaskListViewController.swift
//  Visilean Pro
//
//  Created by VisiLean on 24/06/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit
import BetterSegmentedControl

class TaskListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var viewSegment: GradientView!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var segmentViewHeight: NSLayoutConstraint!
    static let  SegmentCell = "SegmentCell"
    @IBOutlet weak var CollectionSegment: UICollectionView!
    var selectedIndex:Int = 0
    var visibleitems: CGFloat = 3.5

    var items: [String] = ["All Tasks","Make Ready","In Progress","Rejected","QC"]

    @IBOutlet weak var tblTaskList: UITableView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
     self.navBar?.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
     self.navBar?.shadowImage = UIImage()
     self.navBar?.isTranslucent = true
     self.navBar?.backgroundColor = UIColor.clear
     self.navBar?.tintColor = UIColor.white
        var fontsize :CGFloat = 0.0
        if (Device.IS_IPHONE == true)
        {
            fontsize = 12.0
        }
        else
        {
            fontsize = 15.0
        }
    
      
    let viewSegmentedControl = BetterSegmentedControl(
        frame: CGRect(x:0.0, y: 0.0, width: viewSegment.frame.size.width, height: viewSegment.frame.size.height),
            segments: LabelSegment.segments(withTitles: ["Pending", "Approved"],
                                            normalFont:  UIFont(name: fontType.Regular.rawValue, size: fontsize)!,
                                            normalTextColor: colors.colorTabSelectAll,
                                            selectedFont: UIFont(name: fontType.Regular.rawValue, size: fontsize)!,
                                            selectedTextColor:.white ),
            index: 1,
            options: [.backgroundColor(colors.MainBackground),
                      .indicatorViewBackgroundColor(colors.colorTabSelectAll),
                      .cornerRadius(23.0),
                      .animationSpringDamping(1.0),
                      .panningDisabled(true)])
        viewSegment.addSubview(viewSegmentedControl)
        viewSegmentedControl.setIndex(0, animated: false)
        viewSegmentedControl.addTarget(self, action: #selector(TaskListViewController.navigationSegmentedControlValueChanged(_:)),
                                                    for: .valueChanged)
        viewSegmentedControl.frame = CGRect(x:0.0, y: 0.0, width: view.frame.size.width-32, height: viewSegment.frame.size.height)
        
        
     if (Device.IS_IPHONE == true)
    {
        self.navBar?.titleTextAttributes =
                       [NSAttributedString.Key.foregroundColor: UIColor.white,
                        NSAttributedString.Key.font: UIFont(name: fontType.SemiBold.rawValue, size: 18)!]

    }
    else
    {
        self.navBar?.titleTextAttributes =
                       [NSAttributedString.Key.foregroundColor: UIColor.white,
                        NSAttributedString.Key.font: UIFont(name: fontType.SemiBold.rawValue, size: 23)!]

    }
   
//        for i in 0 ... 6 {
//            items.append(i)
//        }
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        self.CollectionSegment.reloadData()
        self.view.layoutIfNeeded()
        self.navigationController?.navigationBar.barStyle = .black;

    }

     override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
        }
//    // MARK: - Collectionview Delegate
     override var preferredStatusBarStyle: UIStatusBarStyle {
         return .lightContent
     }
     func numberOfSections(in collectionView: UICollectionView) -> Int {
         return 1
     }
     
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         
        return items.count

     }

     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SegmentCell", for: indexPath) as! SegmentCell
       
         var fontsize :Int = 0
          var fontsize1 :Int = 0

           if (Device.IS_IPHONE == true)
           {
               fontsize = 14
               fontsize1 = 16

           }
           else
           {
               fontsize = 17
              fontsize1 = 20

           }
        
        if(selectedIndex == indexPath.row)
        {
            if(selectedIndex == 0)
            {
                cell.viewSelector.backgroundColor = colors.colorTabSelectAll
                cell.lblCount.textColor = colors.colorTabSelectAll
                cell.lblName.textColor = colors.colorTabSelectAll
                segmentViewHeight.constant = 0
            }
            else if(selectedIndex == 1)
            {
                cell.viewSelector.backgroundColor = colors.colorTabSelectMakeReady
                cell.lblCount.textColor = colors.colorTabSelectMakeReady
                cell.lblName.textColor = colors.colorTabSelectMakeReady
                segmentViewHeight.constant = 0

            }
            else if(selectedIndex == 2)
            {
                cell.viewSelector.backgroundColor = colors.colorTabSelectInProgress
                cell.lblCount.textColor = colors.colorTabSelectInProgress
                cell.lblName.textColor = colors.colorTabSelectInProgress
                segmentViewHeight.constant = 0

            }
            else if(selectedIndex == 3)
            {
                cell.viewSelector.backgroundColor = colors.colorTabSelectRejected
                cell.lblCount.textColor = colors.colorTabSelectRejected
                cell.lblName.textColor = colors.colorTabSelectRejected
                segmentViewHeight.constant = 0

            }
            else if(selectedIndex == 4)
            {
                cell.viewSelector.backgroundColor = colors.colorTabSelectQC
                cell.lblCount.textColor = colors.colorTabSelectQC
                cell.lblName.textColor = colors.colorTabSelectQC
                segmentViewHeight.constant = 60

            }
            
            cell.lblCount.font = UIFont(name:fontType.SemiBold.rawValue, size: CGFloat(fontsize1))
            cell.lblName.font = UIFont(name:fontType.SemiBold.rawValue, size: CGFloat(fontsize))

            
        }
        else
        {
            cell.viewSelector.backgroundColor = .clear
            cell.lblCount.textColor = colors.colorTabNormal
            cell.lblName.textColor = colors.colorTabNormal
            cell.lblCount.font = UIFont(name:fontType.Regular.rawValue, size: CGFloat(fontsize1))
            cell.lblName.font = UIFont(name:fontType.Regular.rawValue, size: CGFloat(fontsize))
        }
        
        cell.lblCount.text = "\(indexPath.row)"
        cell.lblName.text = "\(items[indexPath.row])"
//        self.CollectionSegment.selectItem(at: IndexPath(row: selectedIndex, section: 0), animated: false, scrollPosition: .centeredHorizontally) // the "of" row should now be highlighted, regardless if the data changed order

         return cell
     }
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        selectedIndex = indexPath.row
        print("Selected :\(indexPath.row)")
        collectionView.reloadData()
        print("contentSize :\(collectionView.contentSize.width)")
        if (Device.IS_IPHONE == true)
        {
           
            let Xformenu = (CGFloat(CollectionSegment.frame.size.width/visibleitems)*CGFloat(selectedIndex)) - CollectionSegment.frame.size.width/1.5
            print("contentSize :\(Xformenu)")

            if (Xformenu > 0)
            {
                self.scrollToNextCell()
            }
            else
            {
                let top = CGPoint(x: 0, y: 0)
                collectionView.setContentOffset(top, animated: true)
            }

        }
       

     }
    func scrollToNextCell(){

        //get Collection View Instance

        //get cell size
        let cellSize = CGSize(width: self.view.frame.width, height: self.view.frame.height);

        //get current content Offset of the Collection view
        let contentOffset = CollectionSegment.contentOffset;

        //scroll to next cell
        CollectionSegment.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);


    }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

    //    CGSize(width: introCollectionView.bounds.size.width, height: introCollectionView.bounds.size.height);
         let  height :CGFloat = CollectionSegment.frame.size.height
        let width :CGFloat  = CollectionSegment.frame.size.width/visibleitems
           // in case you you want the cell to be 40% of your controllers view
         return CGSize(width: width , height: height);
     }
   
    // MARK: - Table View

        func numberOfSections(in tableView: UITableView) -> Int {
           return 10
       }
       func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let headerView = UIView()
        headerView.backgroundColor = colors.MainBackground
        var fontsize :CGFloat = 0.0
        var titleY :CGFloat = 0.0

        if (Device.IS_IPHONE == true)
        {
            fontsize = 12.0
            titleY = 14.0

        }
        else
        {
            fontsize = 15.0
            titleY = 12.5

        }
        let sectionLabel = UILabel(frame: CGRect(x: 16, y: titleY, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        sectionLabel.font = UIFont(name: fontType.SemiBold.rawValue, size: fontsize)
        sectionLabel.textColor = colors.colorTabSelectAll
        sectionLabel.text = "12 Dec 2020 :      \(section)"
        sectionLabel.sizeToFit()
        headerView.addSubview(sectionLabel)

        return headerView
       }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 3
       }

        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
           let cell : TaskCell = tableView.dequeueReusableCell(withIdentifier: "TaskCell") as! TaskCell
          
           cell.lblTitle.text = "Task Names (Manufacturing & Delivery - Acrylic - L1-EO Fabrication of Tracks & Columns) (Manufacturing & Delivery - Acrylic - L1-EO Fabrication of Tracks & Columns)"
           cell.lblLocation.text = "Block B - Floor 5"
           cell.lblDate.text = "Start by : 12 Dec"
            
            if(indexPath.row == 0)
            {
                cell.lblTitle.text = "Task Names"
                cell.lblDate.textColor = colors.colorStartByNormal

                cell.widthSafety.constant = 0
                cell.widthWarning.constant = 25
                cell.widthAlertCount.constant = 25

            }
            else if(indexPath.row == 1)
            {
                cell.widthSafety.constant = 0
                cell.widthWarning.constant = 0
                cell.widthAlertCount.constant = 25
                cell.lblDate.textColor = colors.colorStartByNormal

            }
            else
            {
                 cell.widthSafety.constant = 25
                 cell.widthWarning.constant = 25
                 cell.widthAlertCount.constant = 25
                 cell.lblDate.textColor = colors.colorStartByDateover

            }

           return cell
       }

       func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
           // Return false if you do not want the specified item to be editable.
           return true
       }

//       func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//           if editingStyle == .delete {
//             //  tableView.deleteRows(at: [indexPath], with: .fade)
//           } else if editingStyle == .insert {
//               // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//           }
//       }
       func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
       {
              return UITableView.automaticDimension
       }
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
              return UITableView.automaticDimension
       }
    
    
    @objc func navigationSegmentedControlValueChanged(_ sender: BetterSegmentedControl) {
           if sender.index == 0 {
                 print("pending")
           } else {
            print("approved")
           }
       }
    @IBAction func btnClickMenu(_ sender: Any)
    {
        slideMenuController()?.openLeft()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
