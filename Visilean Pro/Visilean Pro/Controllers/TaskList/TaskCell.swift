//
//  TaskCell.swift
//  Visilean Pro
//
//  Created by VisiLean on 24/06/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit

class TaskCell: UITableViewCell {

    @IBOutlet weak var widthAlertCount: NSLayoutConstraint!
    @IBOutlet weak var widthWarning: NSLayoutConstraint!
    @IBOutlet weak var widthSafety: NSLayoutConstraint!
    @IBOutlet weak var btnSafety: UIButton!
    @IBOutlet weak var btnWarning: UIButton!
    @IBOutlet weak var btnAlertcount: GradientButton!
    @IBOutlet weak var stackofButtons: UIStackView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
