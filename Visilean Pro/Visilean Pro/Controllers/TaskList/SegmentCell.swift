//
//  SegmentCell.swift
//  Visilean Pro
//
//  Created by VisiLean on 25/06/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit

class SegmentCell: UICollectionViewCell {
    
    @IBOutlet weak var viewSelector: UIView!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblName: UILabel!
}
