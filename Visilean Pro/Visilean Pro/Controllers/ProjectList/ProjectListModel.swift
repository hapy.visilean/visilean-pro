//
//  ProjectListModel.swift
//  Visilean Pro
//
//  Created by VisiLean on 29/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ProjectListResponseModel  {

    var result = Array<JSON>()
    var Status = 0
    var Message = ""
   
    
    init(data: JSON) {
        self.result = data["result"].array!
        self.Status = data["status"].int!
        self.Message = data["message"].string!
       
    }
}
struct ProjectListModel {

    var Id = 0
    var guid = 0
    var name = ""
    var description = ""
    var projectId = ""
    var startDate = ""
    var endDate = ""
    var actualStartDate = ""
    var actualEndDate = ""
    var baselineStartDate = ""
    var baselineEndDate = ""
    var siteLocation = ""
    var projectTimeZone = ""
    var bimserverPoid = ""
    var projectRoleHolderRelations = Array<JSON>()
    var projectActivities = Array<JSON>()
    var projectLocations = Array<JSON>()
    var isWorkflowEnforced = false
  

    
    init(data: JSON) {
        
        self.Id = data["@id"].int ?? 0
        self.guid = data["guid"].int ?? 0
        self.name = data["name"].string ?? ""
        self.description = data["description"].string ?? ""
        self.projectId = data["projectId"].string ?? ""
        self.startDate = data["startDate"].string ?? ""
        self.endDate = data["endDate"].string ?? ""
        self.actualStartDate = data["actualStartDate"].string ?? ""
        self.actualEndDate = data["actualEndDate"].string ?? ""
        self.baselineStartDate = data["baselineStartDate"].string ?? ""
        self.baselineEndDate = data["baselineEndDate"].string ?? ""
        self.siteLocation = data["siteLocation"].string ?? ""
        self.projectTimeZone = data["projectTimeZone"].string ?? ""
        self.bimserverPoid = data["bimserverPoid"].string ?? ""
        self.projectRoleHolderRelations = data["projectRoleHolderRelations"].array ?? []
        self.projectActivities = data["projectActivities"].array ?? []
        self.projectLocations = data["projectLocations"].array ?? []
        self.isWorkflowEnforced = data["isWorkflowEnforced"].bool ?? false
    }
}
