//
//  ProjectListViewModel.swift
//  Visilean Pro
//
//  Created by VisiLean on 29/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ProjectListViewModelProtocol {

var getProjectListData: ((Bool, Bool) -> Void)? { get set }
}

class ProjectListViewModel : ProjectListViewModelProtocol
{
var getProjectListData: ((Bool, Bool) -> Void)?

    private var projectDataModel: ProjectListResponseModel? = nil

var projectListModel: [ProjectListModel]? {
    didSet {
        self.getProjectListData!(true, false)
    }
}

 func viewDidLoad() {

    configureListModel()
}

private func configureListModel() {

    var userId:String = ""
    if UserDefaults.exists(key: "userDic") == true
    {
        let userDic :NSDictionary = Defaults.shared.value(forKey: "userDic") as! NSDictionary
        userId = userDic.value(forKey: "userId") as! String
    }
    
       print(userId)
    APIManager.sharedInstance.getProjectList(actorGuid:userId ){
        (response,error) in
    
        if response != nil
        {
            print(" \nResponse: \(response)")
            self.projectDataModel = ProjectListResponseModel(data: response!)
            if self.projectDataModel!.Status == 1
            {
                self.configureOutput()
            }
            else
            {
                self.configureError()
            }
        }
        
        
    }
     
}
  
   //Configure the output properties that are to be accessed by the view.
private func configureOutput() {
    
    var dataobj : [ProjectListModel] = []
  
    for i in 0 ..< projectDataModel!.result.count
    {
        let dict = ProjectListModel(data: projectDataModel!.result[i])
        print(dict)
        dataobj.append(dict)
    }
    
     self.projectListModel = dataobj
    //print(self.loginModel!.ModifiedDate)
     
}


private func configureError()
{
    print(projectDataModel!.Message)
}

}
