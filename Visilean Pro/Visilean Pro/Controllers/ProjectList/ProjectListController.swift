//
//  ProjectListController.swift
//  Visilean Pro
//
//  Created by VisiLean on 18/06/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit
import AlamofireImage

class ProjectListController: UIViewController, iCarouselDataSource, iCarouselDelegate
{
    private var projectListViewModel = ProjectListViewModel()

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var rightPadding: NSLayoutConstraint!
    @IBOutlet weak var leftPadding: NSLayoutConstraint!
    var padding :Int = 0
    var multiply :CGFloat = 0.0
    @IBOutlet weak var pdLeft: NSLayoutConstraint!
    @IBOutlet weak var pdRight: NSLayoutConstraint!
    
    var items: [Int] = []
    var taskActivitylist: [String] = ["Completed","Alert Tasks","Stopped Tasks","Last Week PPC"]
    var taskActivityImages: [String] = ["task_completed_icon","task_alert_icon","task_stopped_icon","task_lastweek_icon"]
    var arrColorProjectCard: [UIColor] = [colors.colorProjectLbl1,colors.colorProjectLbl2,colors.colorProjectLbl3,colors.colorProjectLbl4]

    @IBOutlet var carousel: iCarousel!
    override func awakeFromNib() {
            super.awakeFromNib()
            for i in 0 ... 10 {
                items.append(i)
            }
        if (Device.IS_IPHONE == true)
               {
                  padding = 40
                  
               }
               else
               {
                   padding = 130
               }
               
              
        }

    override func viewDidLoad() {
    super.viewDidLoad()
    carousel.type = .invertedTimeMachine
    carousel.isVertical = true
    carousel.bounces = false
    carousel.isPagingEnabled = false
    carousel.isScrollEnabled = true

    if (Device.IS_IPHONE == true)
    {
        carousel.perspective = -1/1000

    }
    else
    {
        carousel.perspective = -1/2000
    }
                      
        carousel.scrollSpeed = -1.0

    leftPadding.constant = CGFloat(padding)
    rightPadding.constant = CGFloat(padding)
        
       projectListViewModel.viewDidLoad()
       setUpUI()
        
    }
    private func setUpUI() {
        self.projectListViewModel.getProjectListData = { (finished, error) in
        if !error {
                   print(self.projectListViewModel.projectListModel!.count as Any)
                    self.carousel.reloadData()
                  }
              }
       }
       
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if UserDefaults.exists(key: "userDic") == true
        {
        let userDic :NSDictionary = Defaults.shared.value(forKey: "userDic") as! NSDictionary
        lblUsername.text = userDic.value(forKey: "username") as? String
        imgUser.af.setImage(withURL: URL(fileURLWithPath: ""), placeholderImage : UIImage(named: "user_placeholder") )
        }
        
//        navigationController?.navigati onBar.barStyle = .blackTranslucent
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
   // MARK: - iCarousel DataSource & Deligates
    
    
    func numberOfItems(in carousel: iCarousel) -> Int {
    return self.projectListViewModel.projectListModel?.count ?? 4
   }

   func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {

    let screenSize: CGRect = UIScreen.main.bounds
    let screenWidth = screenSize.width
    //        let screenHeight = screenSize.height
    var cornerRadius :CGFloat = 0
    if (Device.IS_IPHONE == true)
    {
        cornerRadius = 25
    }
    else
    {
        cornerRadius = 40
    }
    var img: UIImageView
    var img_titleBg: UIImageView
    var itemView: UIImageView
     let mainView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth - CGFloat((2 * padding)), height: (2.5 * (screenWidth - CGFloat((2 * padding))))/2))
       mainView.backgroundColor = UIColor.white
       mainView.layer.shadowColor = UIColor.black.cgColor
       mainView.layer.shadowOffset = CGSize(width: 0, height: 3)
       mainView.layer.shadowOpacity = 0.12
       mainView.layer.shadowRadius = 6
    mainView.layer.cornerRadius = cornerRadius

      let tempView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth - CGFloat((2 * padding)), height: (2.5 * (screenWidth - CGFloat((2 * padding))))/2))
     tempView.clipsToBounds = true
    tempView.layer.cornerRadius = cornerRadius

    itemView = UIImageView(frame: CGRect(x: 0, y: 0, width: screenWidth - CGFloat((2 * padding)), height: 1.8*tempView.frame.height/3))
    itemView.image = UIImage(named: "\(index)")
    itemView.contentMode = .scaleAspectFill
    
    var updatedYpos = 1.8*tempView.frame.height/3
    
    var bottomView = UIView.init()
    for i in 0 ..< taskActivityImages.count
    {
      
        var Height :Int = 0
        if (Device.IS_IPHONE == true)
        {
            Height = 14
        }
        else
        {
            Height = 17
        }
        if i > 0
        {
            updatedYpos = updatedYpos + CGFloat(Height)+15
        }
        
        bottomView = UIView.init(frame: CGRect.init(x: 12, y: updatedYpos+10, width: screenWidth - CGFloat(2 * padding), height: CGFloat(Height+10)))
        img = UIImageView(frame: CGRect(x: 0, y: 5, width: bottomView.frame.size.height-10, height:bottomView.frame.size.height-10))
        img.image = UIImage(named: taskActivityImages[i])
        
       
        
        
        let label1 = UILabel(frame:  CGRect(x: img.frame.size.width+10, y: 0 , width: bottomView.frame.size.width-60-img.frame.size.width , height: bottomView.frame.size.height))
        label1.textAlignment = .left
        label1.textColor = colors.PrimaryBlue
        label1.font = UIFont(name:fontType.Regular.rawValue, size: CGFloat(Height))
        label1.tag = i + 1
        label1.text =  taskActivitylist[i]
        
        let lblLast = UILabel(frame:  CGRect(x: label1.frame.size.width , y: 0 , width: 50, height: bottomView.frame.size.height))
        lblLast.textAlignment = .center
        lblLast.font = UIFont(name:fontType.Regular.rawValue, size: CGFloat(Height))
        lblLast.tag = 100*i + 1
        lblLast.textColor = arrColorProjectCard[i]
        lblLast.text =  "\(items[i])"
        bottomView.addSubview(img)
        bottomView.addSubview(label1)
        bottomView.addSubview(lblLast)
        tempView.addSubview(bottomView)
        
     }
    var fontsize :Int = 0
    var fontsizeT :Int = 0
    var hightBlackBG :Int = 0
    var TitlePadding :Int = 0

    if (Device.IS_IPHONE == true)
    {
        fontsize = 14
        fontsizeT = 24
        hightBlackBG = 100
        TitlePadding = 30

    }
    else
    {
        fontsize = 19
        fontsizeT = 30
        hightBlackBG = 130
        TitlePadding = 40

    }
    
    img_titleBg = UIImageView(frame: CGRect(x: 0, y: Int(CGFloat(Int(itemView.frame.size.height)-hightBlackBG)), width: Int(screenWidth - CGFloat((2 * padding))), height:hightBlackBG))
    img_titleBg.image = UIImage(named: "name_bg")
    
    let lblLocation = UILabel(frame:  CGRect(x: 12, y: Int(itemView.frame.size.height)-(fontsize + 20) , width: Int(screenWidth - CGFloat((2 * padding))) , height: fontsize + 10))
    lblLocation.textAlignment = .left
    lblLocation.font = UIFont(name:fontType.Medium.rawValue, size: CGFloat(fontsize))
    lblLocation.tag = index
    lblLocation.textColor = .white
    lblLocation.text =  self.projectListViewModel.projectListModel?[index].siteLocation
        
    let YforTitle = Int(itemView.frame.size.height)-((fontsize + TitlePadding) + Int(lblLocation.frame.size.height))
    
    let lblTitle = UILabel(frame:  CGRect(x: 12, y: YforTitle , width: Int(screenWidth - CGFloat((2 * padding))) , height: fontsize + 20))
    lblTitle.textAlignment = .left
    lblTitle.font = UIFont(name:fontType.Bold.rawValue, size: CGFloat(fontsizeT))
    lblTitle.tag = index
    lblTitle.textColor = .white
    lblTitle.text =  self.projectListViewModel.projectListModel?[index].name
        
    tempView.addSubview(itemView)
    tempView.addSubview(img_titleBg)
    tempView.addSubview(lblTitle)
    tempView.addSubview(lblLocation)
    mainView.addSubview(tempView)

    return mainView

   }
   func carousel(_ carousel: iCarousel, didSelectItemAt index: Int)
   {
    
        print("selected index : \(index)")
        Application.Delegate.makeRootStandardUser()
    //let VC = UIStoryboard.TaskList()
    //self.navigationController?.pushViewController(VC, animated: true)
    
    
    
    }
//    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
//        //you need to reload carousel for update view of current index
//        self.carousel.reloadData()
//
//
//    }

   func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
   {
   

    if (option == .tilt) {
        if (Device.IS_IPHONE == true)
        {
//            return value*(-1.5)
            switch UIScreen.main.bounds.size.height {
                   case 568:
//                       print("iPhone 5 or 5S or 5C")
                       multiply = -0.7

                   case 667:
//                       print("iPhone 6/6S/7/8")
                       multiply = -0.8

                   case 736:
//                       print("iPhone 6+/6S+/7+/8+")
                       multiply = -0.9
                   case 812:
//                       print("iPhone X, XS")
                       multiply = -0.8
                   case 896:
//                       print("iPhone XMax, XSMax")
                        multiply = -0.9
                   default:
//                       print("Unknown")
                     multiply = -0.7
                     print(UIScreen.main.bounds.size.height)

                   }
            return value*multiply

        }
        else
        {
            
            switch UIScreen.main.bounds.size.height {
                  case 1024:
                    multiply = -0.65
//                    print("iPad 9.7 inch/Ratina")
                  case 1080:
                   multiply = -0.7
//                    print("iPad air 3rd")
                case 1112:
                    multiply = -0.8
//                    print("iPad 7th")
                 case 1194:
                    multiply = -0.85
//                    print("iPad 11 inch/Ratina")
                  case 1366:
                    multiply = -0.95
//                    print("iPad 12.9 inch/Ratina")
                  default:
//                    print("Unknown")
                    multiply = -0.65
                    print(UIScreen.main.bounds.size.height)
                  }
            
            return value*multiply
        }
    }
    return value
   }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
