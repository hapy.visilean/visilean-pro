//
//  APIManager.swift
//  Demo VisileanPRO
//
//  Created by Gopal on 09/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias ResponseHandler = (Dictionary<String, AnyObject>?, NSError?) -> Void

class APIManager {
    
    // Class Stored Properties
    internal typealias CompletionBlock = (_ data:JSON?,_ error:NSError?) -> Void
    
    static let sharedInstance = APIManager()

    private init() {}
  
    
    // MARK: HTTPHeaders

    func headers() -> HTTPHeaders {
       
        UserDefaults.standard.synchronize()
        let token = UserDefaults.standard.object(forKey: "TOKEN") as? String ?? ""
        
        let headers: HTTPHeaders = [
            "x-auth-token": token,
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        return headers
    }
    
    // MARK: Alamofire Request Manager
    func requestAPI(url: URL, method : HTTPMethod,parameters : Parameters?,headers :HTTPHeaders?,completionHandler: @escaping CompletionBlock) {
        print(url as Any)
        print(method as Any)
        print(parameters as Any)
        //print(headers! as HTTPHeaders)

        AF.request(url, method: method, parameters: parameters, encoding: URLEncoding(), headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in

            print(response.request as Any)  // original URL request
            print(response.response as Any) // URL response
            print(response.data as Any)     // server data
            print(response.result)   // result of response serialization
            print(response.value as Any)
            print(response.error as Any)

            switch response.result {
                   case .success(let value):
                       let statuscode = response.response?.statusCode
                       
                       if statuscode == 200
                       {
                            let swiftyJsonVar = JSON(value)
                            print(" \nResponse: \(swiftyJsonVar)")
                            completionHandler(swiftyJsonVar, nil)
                       }
                       

                    case .failure(let error):
                       print("Error while querying database: \(String(describing: error))")
                       print(error.errorDescription ?? "")
                       print(response.response?.statusCode as Any)
                       self.ErrorHandler(errorCode: response.response?.statusCode ?? 0)
                       completionHandler(nil, error as NSError)
                       return
                
                    }


        }
    }

    func userLogin(username: String, password : String ,completionHandler: @escaping CompletionBlock)
    {
        let params : [String : String]  = [
                   "Emailid": username,
                   "Password": password]
        
        print(APIConfig.API_URL_login?.absoluteString as Any)
        self.requestAPI(url: APIConfig.API_URL_login!, method: .post, parameters: params, headers: headers())
        { (response, error) in
                completionHandler(response,error)
        }
    }
    
    func requestList(completionHandler: @escaping CompletionBlock) {
    
        //http://api.moonshinefoodstuff.com//api/ServiceProviderbySubcategory?SubcategoryId=1&searchtext=
         let API_URL_list = URL(string: "http://api.moonshinefoodstuff.com//api/ServiceProviderbySubcategory?SubcategoryId=1&searchtext=")
        self.requestAPI(url: API_URL_list!, method: .get, parameters: nil, headers: headers())
        { (response, error) in
            completionHandler(response,error)
        }
    }
    
    
    func userStage(registrationToken: String ,stage : Int ,forename : String ,organisationToAddTo : String, password : String ,projectToRegisterInId : String , roleToApplyId : String ,salutation : String , surname : String ,username : String , completionHandler: @escaping CompletionBlock) {

        let registrationDetails : [String : Any] = [
                    "forename" : forename,
                    "organisationToAddTo" : organisationToAddTo,
                    "password" : password,
                    "projectToRegisterInId" : projectToRegisterInId,
                    "registrationToken" : registrationToken,
                    "roleToApplyId" : roleToApplyId,
                    "salutation" : salutation,
                    "stage" : stage,
                    "surname" : surname,
                    "username" : username
        ]
        
        let params : [String : Any]  = [
                  "registrationToken": registrationToken,
                  "stage" : stage,
                  "registrationDetails" : registrationDetails,
                  
        ]
        
        let strMethod = String(format: "%@/%@" , APIConfig.BaseURL , urlPath.postRegisterUser.rawValue)
        let api_url = URL(string: strMethod)!

        self.requestAPI(url: api_url, method: .post, parameters: params, headers: self.headers())
        { (response, error) in
               completionHandler(response,error)
        }
    }
    
    
    func registerUser(orgGuid: String ,projectGuid : String ,roleName : String ,username : String, password : String , sendEmail : Bool ,  completionHandler: @escaping CompletionBlock) {

        let params : [String : Any]  = [
                  "orgGuid": orgGuid,
                  "projectGuid" : projectGuid,
                  "roleName" : roleName,
                  "username" : username,
                  "password" : password,
                  "sendEmail" : sendEmail
        ]
        
        let strMethod = String(format: "%@/%@" , APIConfig.BaseURL , urlPath.postRegisterUser.rawValue)
        let api_url = URL(string: strMethod)!

        self.requestAPI(url: api_url, method: .post, parameters: params, headers: self.headers())
        { (response, error) in
               completionHandler(response,error)
        }
    }
    
    func createProject(description: String ,endDate : String ,isWorkflowEnforced : Bool ,name : String, projectId : String , projectTimeZone : String , siteLocation: String , startDate : String,  completionHandler: @escaping CompletionBlock) {

        let params : [String : Any]  = [
                  "description": description,
                  "startDate" : startDate,
                  "endDate" : endDate,
                  "isWorkflowEnforced" : isWorkflowEnforced,
                  "name" : name,
                  "projectId" : projectId,
                  "projectTimeZone" : projectTimeZone,
                  "siteLocation" : siteLocation
        ]
        
        let strMethod = String(format: "%@/%@" , APIConfig.BaseURL , urlPath.postCreateProject.rawValue)
        let api_url = URL(string: strMethod)!

        self.requestAPI(url: api_url, method: .post, parameters: params, headers: self.headers())
        { (response, error) in
               completionHandler(response,error)
        }
    }
    
    func resetPassword(username: String , completionHandler: @escaping CompletionBlock) {

        //https://api.visilean.com/VisileanAPI/api/reset/password?username=
        
        let params : [String : String]  = [
                  "username": username
        ]
        
        let strMethod = String(format: "%@%@" , APIConfig.BaseURL , urlPath.getResetPassword.rawValue)
        let api_url = URL(string: strMethod)!

        self.requestAPI(url: api_url, method: .get, parameters: params, headers: nil)
        { (response, error) in
               completionHandler(response,error)
        }
    }
    
    func getProjectList(actorGuid: String , completionHandler: @escaping CompletionBlock) {

        
        let strMethod = String(format: "%@/%@actorGuid=%@" , APIConfig.BaseURL , urlPath.getProjects.rawValue , actorGuid)
        let api_url = URL(string: strMethod)!

        
        self.requestAPI(url: api_url, method: .get, parameters: nil, headers: self.headers())
        { (response, error) in
               completionHandler(response,error)
        }
    }
    
    func getCountryList( completionHandler: @escaping CompletionBlock) {

        let strMethod = String(format: "%@/%@" , APIConfig.BaseURL , urlPath.getCountryList.rawValue)
        let api_url = URL(string: strMethod)!

        self.requestAPI(url: api_url, method: .get, parameters: nil, headers: self.headers())
        { (response, error) in
               completionHandler(response,error)
        }
    }
    
    func getRoleList(username: String , completionHandler: @escaping CompletionBlock) {

        let strMethod = String(format: "%@/%@" , APIConfig.BaseURL , urlPath.getRoles.rawValue)
        let api_url = URL(string: strMethod)!

        self.requestAPI(url: api_url, method: .get, parameters: nil, headers: self.headers())
        { (response, error) in
            completionHandler(response,error)
        }
        
    }
    
    func requestGetAndUpdateActivityDateWiseWorkers(actual: String, date : String, planned : String, isPlanned : String, type : String,guid : String,completionHandler: @escaping CompletionBlock) {

             let params : [String : String]  = [
                      "actual": actual,
                      "date": date,
                      "planned": planned,
                      "isPlanned": isPlanned,

             ]
        let API_URL_getAndUpdateActivityDateWiseWorkers = URL(string: "\(APIConfig.BaseURL)/\(guid)/\(urlPath.Method_getAndUpdateActivityDateWiseWorkers.rawValue)/\(type)")

        self.requestAPI(url: API_URL_getAndUpdateActivityDateWiseWorkers!, method: .post, parameters: params, headers: self.headers())
        { (response, error) in
            completionHandler(response,error)
        }
        
    }
    
}


extension APIManager
{
    func ErrorHandler(errorCode: Int) {
        // find url from url from url.absoluteString
        
        switch errorCode
        {
            case 403 : print("User not have a rights")
                   break
            case 401 : print("error is 401")
        
            UserAuthentication.sharedInstance.userLoginAuthenticate()
            
            print("Session expired")
            break
            
        default:
            print("default")
        }
        
    }
}
