//
//  DicConvertor.swift
//  Visilean Pro
//
//  Created by hapy gohil on 16/06/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import Foundation

class ConvertManager : NSObject
{
    func convertLoginModelToDic(model : LoginResponseModel) -> NSDictionary
    {
    
        let arr = NSMutableArray()
        for i in 0 ..< model.authorities.count
        {
            let authorities =  model.authorities[i]
            
            let innerDic = NSMutableDictionary()
            innerDic.setValue(authorities["name"].string, forKey: "name")
            innerDic.setValue(authorities["roleScope"].string, forKey: "roleScope")
            innerDic.setValue(authorities["uiName"].string, forKey: "uiName")
            
            arr.add(innerDic)
        }
        
        
        
        
        let newDic = NSMutableDictionary()
        newDic.setValue(model.accountNonExpired, forKey: "accountNonExpired")
        newDic.setValue(model.accountNonLocked, forKey: "accountNonLocked")
        newDic.setValue(model.credentialsNonExpired, forKey: "credentialsNonExpired")
        newDic.setValue(model.enabled, forKey: "enabled")
        newDic.setValue(model.password, forKey: "password")
        newDic.setValue(model.userId, forKey: "userId")
        newDic.setValue(model.username, forKey: "username")
        newDic.setValue(arr, forKey: "authorities")
     
        return newDic.mutableCopy() as! NSDictionary
    }
    
}
