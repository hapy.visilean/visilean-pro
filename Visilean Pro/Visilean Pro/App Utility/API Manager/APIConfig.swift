//
//  APIConfig.swift
//  Demo VisileanPRO
//
//  Created by Gopal on 09/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import Foundation


struct APIConfig {
    
    // MARK: Base URL

   #if DEBUG
       static let BaseURL = "https://api.visilean.com/VisileanAPI/"

    //static let BaseURL = "http://api.moonshinefoodstuff.com/api/"
    
    #else
       static let BaseURL = "https://go.visilean.com/VisileanAPI/"
    #endif
           
    // MARK: All API URLS
    static let API_URL_login = URL(string: "\(BaseURL)\(urlPath.Method_login.rawValue)")
  

}
// MARK: All API Methods
enum urlPath: String {
    case Method_login = "RegisterLogin/UserLogin"
    case Method_getAndUpdateActivityDateWiseWorkers = "getAndUpdateActivityDateWiseWorkers"
    case getProjects = "api/actor/projects?"
    case addActor = "api/actor/newOrg/keyContact/add"
    case getResetPassword = "api/reset/password?"
    case getCountryList = "api/project/countryList"
    case getRoles = "api/project/roles"
    case postCreateProject = "api/project/create"
    case postRegisterUser = "api/register/registration/user"
    case postUserStage = "api/register/stage"
}

//// MARK: All API Header
//enum HTTPHeaderField: String {
//    case authToken = "x-auth-token"
//    case userAgent = "User-Agent"
//    case contentType = "Content-Type"
//    case Accept = "Accept"
//
//}

// MARK: All API ContentType
enum ContentType: String {
    case json = "application/json"
    case phone = "visilean-ios-native/phone"
    case token = "b834aa27-80da-47ca-bd14-0c44f4beec59"

}


