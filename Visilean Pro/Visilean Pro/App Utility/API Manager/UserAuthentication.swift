//
//  UserAuthentication.swift
//  Visilean Pro
//
//  Created by hapy gohil on 29/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class UserAuthentication {
    
    static let sharedInstance = UserAuthentication()

    private init() {}
    
    func userLoginAuthenticate()
        {
            if UserDefaults.exists(key: "email") == true && UserDefaults.exists(key: "password") == true
            
            {
                let email = UserDefaults.standard.object(forKey: "email") as! String
                let pass = UserDefaults.standard.object(forKey: "password") as! String
                
                let strMethod = String(format: "%@authenticate?password=%@&username=%@",APIConfig.BaseURL ,pass,email)

                print(strMethod)
                let url = URL.init(string: strMethod)
                
                AF.request(url!, method: .post, parameters: nil, encoding: URLEncoding(), headers: nil)
                    .validate(statusCode: 200..<300)
                    .responseJSON { (response) in

                        if let headers = response.response?.allHeaderFields as? [String : String]
                        {
                            if let header = headers["x-auth-token"] as? String
                            {
                                if header != ""
                                {
                                    UserDefaults.standard.set(header, forKey: "TOKEN")
                                    UserDefaults.standard.synchronize()
                                    print(header)
                                }
                            }
                        }
                             
                        let statuscode = response.response?.statusCode
                        if statuscode == 401
                        {
                            print("invalid credential")
                            Application.Delegate.gotoLoginPage()
                            return
                        }
                        //print(response.request as Any)  // original URL request
                        print(response.response as Any) // URL response
                        print(response.data as Any)     // server data
                        print(response.result)   // result of response serialization
                        print(response.value as Any)
                        print(response.error as Any)

                        switch response.result {
                            case .success(let value):
                                let dic = value as! [String : Any]
                                
                                print(dic)
                                let swiftyJsonVar = JSON(value)
                                //print(" \nResponse: \(swiftyJsonVar)")
                                if statuscode == 200
                                {
                                    
                                    let userModel = LoginResponseModel.init(data: swiftyJsonVar)
                                    
                                    print(userModel)
                                    let userDic = ConvertManager().convertLoginModelToDic(model: userModel)
                                    UserDefaults.standard.set(userDic, forKey: "userDic")
                                    UserDefaults.standard.set("", forKey: "projectGuid")
                                    Application.Delegate.makeRootProjectList()
                                }
                            
                            case .failure(let error):
                                print("Error while querying database: \(String(describing: error))")
                                print(error.errorDescription ?? "")
                                print(response.response?.statusCode as Any)
                                Application.Delegate.gotoLoginPage()
                                return
                                 
                        }
                    }
            }
        }
}
