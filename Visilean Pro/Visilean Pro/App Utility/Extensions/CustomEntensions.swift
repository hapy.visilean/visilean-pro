//
//  CustomEntensions.swift
//  Visilean Pro
//
//  Created by hapy gohil on 13/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//


import Foundation
import UIKit

extension UserDefaults {
    static func exists(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
}

extension UIColor {
    convenience init(colorCode: String, alpha: Float = 1.0){
        let scanner = Scanner(string:colorCode)
        var color:UInt32 = 0;
        scanner.scanHexInt32(&color)

        let mask = 0x000000FF
        let r = CGFloat(Float(Int(color >> 16) & mask)/255.0)
        let g = CGFloat(Float(Int(color >> 8) & mask)/255.0)
        let b = CGFloat(Float(Int(color) & mask)/255.0)

        self.init(red: r, green: g, blue: b, alpha: CGFloat(alpha))
    }
}


/*
extension UIColor {
    static func appBackgroundColor() -> UIColor {
        return UIColor(red: 188.0/255.0, green: 205.0/255.0, blue: 182.0/255.0, alpha: 1.0)
    }
    
    public convenience init(hex: String) {
               let r, g, b, a: CGFloat

               if hex.hasPrefix("#") {
                   let start = hex.index(hex.startIndex, offsetBy: 1)
                   let hexColor = String(hex[start...])

                   if hexColor.count == 8 {
                       let scanner = Scanner(string: hexColor)
                       var hexNumber: UInt64 = 0

                       if scanner.scanHexInt64(&hexNumber) {
                           r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                           g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                           b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                           a = CGFloat(hexNumber & 0x000000ff) / 255

                           self.init(red: r, green: g, blue: b, alpha: a)
                           return
                       }
                   }
               }

               return nil
        }
}
*/


extension UIButton
{
    
    func makeDisable()
    {
        self.isUserInteractionEnabled = false
        self.alpha = 0.5
    }
    
    func makeEnable()
    {
        self.isUserInteractionEnabled = true
        self.alpha = 1.0
    }

}

//extension UIButton {
//    func drawShadow(shadowColor: UIColor)
//    {
//        //layoutIfNeeded()
//        self.layer.shadowRadius = 3
//        self.layer.shadowColor = shadowColor.cgColor
//        self.layer.shadowOffset = .zero
//        self.layer.shadowOpacity = 1
//        self.layer.masksToBounds = false
//        updateShadow()
//    }
//    
//    func updateShadow() {
//         self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//    }
//    
//    func makeRoundedCornerButton(Radius radius : CGFloat)
//    {
//        self.layer.cornerRadius = self.frame.height / 2;
//        self.layer.masksToBounds = true;
//    }
//    public var title: String? {
//        get { return self.title(for: .normal) }
//        set { setTitle(newValue, for: .normal) }
//    }
//    
//    public var titleFont: UIFont? {
//        get { return titleLabel?.font }
//        set { titleLabel?.font = newValue }
//    }
//    
//    public var attributedTitle: NSAttributedString? {
//        get { return self.attributedTitle(for: .normal) }
//        set { setAttributedTitle(newValue, for: .normal) }
//    }
//    
//    public var titleColor: UIColor? {
//        get { return self.titleColor(for: .normal) }
//        set {
//            setTitleColor(newValue, for: .normal)
//            setTitleColor(newValue?.withAlphaComponent(0.5), for: .disabled)
//            setTitleColor(newValue, for: .selected)
//            if buttonType == .custom {
//                setTitleColor(newValue?.withAlphaComponent(0.5), for: .highlighted)
//            }
//        }
//    }
//    
//    public var titleShadowColor: UIColor? {
//        get { return self.titleShadowColor(for: .normal) }
//        set {
//            setTitleShadowColor(newValue, for: .normal)
//            setTitleShadowColor(newValue?.withAlphaComponent(0.5), for: .disabled)
//            setTitleShadowColor(newValue, for: .selected)
//        }
//    }
//    
//    public var image: UIImage? {
//        get { return self.image(for: .normal) }
//        set {
//            setImage(newValue?.withRenderingMode(.alwaysOriginal), for: .normal)
//        }
//    }
//    
//    public var selectedImage: UIImage? {
//        get { return self.image(for: .selected) }
//        set { setImage(newValue?.withRenderingMode(.alwaysOriginal), for: .selected) }
//    }
//    
//    public var backgroundImage: UIImage? {
//        get { return self.backgroundImage(for: .normal) }
//        set {
//            let image = newValue?.withRenderingMode(.alwaysOriginal)
//            setBackgroundImage(image, for: .normal)
//        }
//    }
//    
//    public var selectedBackgroundImage: UIImage? {
//        get { return self.backgroundImage(for: .selected) }
//        set { setBackgroundImage(newValue?.withRenderingMode(.alwaysOriginal), for: .selected) }
//    }
//    
//    public var disabledBackgroundImage: UIImage? {
//        get { return self.backgroundImage(for: .disabled) }
//        set { setBackgroundImage(newValue?.withRenderingMode(.alwaysOriginal), for: .disabled) }
//    }
//    public func performToggleSelectStateImageAnimation() {
//        guard let normalImage = self.image(for: .normal) else { return }
//        guard let selectedImage = self.image(for: .selected) else { return }
//        guard let _imageView = imageView else { return }
//        
//        // Clear image
//        {
//            setImage(nil, for: .normal)
//            setImage(nil, for: .selected)
//        }()
//        
//        let animatedImageView = UIImageView(image: isSelected ? selectedImage : normalImage)
//        animatedImageView.frame = _imageView.frame
//        addSubview(animatedImageView)
//        
//        let recover = {
//            UIView.animate(withDuration: 0.2, delay: 0, options: .beginFromCurrentState, animations: {
//                animatedImageView.transform = CGAffineTransform.identity
//            }, completion: { (finished: Bool) in
//                self.setImage(normalImage, for: .normal)
//                self.setImage(selectedImage, for: .selected)
//                self.isSelected = !self.isSelected
//                animatedImageView.removeFromSuperview()
//            })
//        }
//        
//        let zoomOut = {
//            animatedImageView.image = !self.isSelected ? selectedImage : normalImage
//            UIView.animate(withDuration: 0.2, delay: 0, options: .beginFromCurrentState, animations: {
//                animatedImageView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
//            }, completion: { (finished: Bool) in
//                recover()
//            })
//        }
//        
//        // Start with zoom in
//        UIView.animate(withDuration: 0.2, delay: 0, options: .beginFromCurrentState, animations: {
//            animatedImageView.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
//        }, completion: { (finished: Bool) in
//            zoomOut()
//        })
//    }
//}
//

extension UIView {
    func makeRoundedCornerView(Radius radius : CGFloat)
    {
       
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.8
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 5.0
        self.layer.masksToBounds = false
        
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true;
        
        self.addShadow()
    }
    
    func addShadow() {
        self.backgroundColor = UIColor.white
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.8
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 6.0
        self.layer.masksToBounds = false
    }
    public var borderWith: CGFloat {
        get { return layer.borderWidth }
        set { layer.borderWidth = newValue }
    }
    
    /// Setup border width & color.
    @available(iOS 13.0, *)
    public func setBorder(
        width: CGFloat = 1.0 / UIScreen.main.scale,
        color: UIColor = UIColor.separator)
    {
        layer.borderWidth = width
        layer.borderColor = color.cgColor
    }
}

extension UITextField {
    func makeRoundedCornerTextField( Radius radius : CGFloat , borderColor : UIColor , padding : Int)
    {
        self.backgroundColor = .white
        let newView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: padding, height: Int(self.frame.size.height)))
        self.leftView = newView
        self.leftViewMode = .always
        self.layer.cornerRadius = radius
        self.layer.borderWidth = 1.0
        self.layer.borderColor = borderColor.cgColor
        self.layer.masksToBounds = true;
    }
    
    
    
    func addPaddingLeft(Size size  : CGFloat)
    {
        let newView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 20, height: self.frame.height))
        self.leftView = newView
        self.leftViewMode = .always
    }
}

extension UITextView {
    func makeRoundedCornerTextView( Radius radius : CGFloat)
    {
        //let newView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 20, height: self.frame.height))
        //self.leftView = newView
        //self.leftViewMode = .always
        self.layer.cornerRadius = radius
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.masksToBounds = true;
        self.textContainerInset = UIEdgeInsets(top: 10, left: 20, bottom: 0, right: 10)
    }
    
    func addPaddingLeft(Size size  : CGFloat)
    {
        //let newView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 20, height: self.frame.height))
        //self.leftView = newView
        //self.leftViewMode = .always
    }
    
    
}

extension UIFont
{
    func setFontPoppins(size : CGFloat , fontType : fontType) -> UIFont
    {
        let font = UIFont.init(name: fontType.rawValue, size: size)
        return font!
    }
}

enum fontType: String {
    case Bold = "Poppins-Bold"
    case Regular = "Poppins-Regular"
    case Medium = "Poppins-Medium"
    case SemiBold = "Poppins-SemiBold"
    

}
