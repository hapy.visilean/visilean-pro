//
//  Toast.swift
//  Visilean Pro
//
//  Created by Gopal on 18/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import Foundation
import UIKit

/// Available toast font sizes
enum ToastSize {
    /// Corresponds to UIFontTextStyle.body
    case small
    /// Corresponds to UIFontTextStyle.title2
    case normal
    /// Corresponds to UIFontTextStyle.title1
    case large
}
enum ToastDuration : TimeInterval {
    case short = 0.6, normal = 2.0, long = 3.5
}

fileprivate class ToastLabel : UILabel { }

extension UIView {
    /// Creates a toast message as a subview of a given UIView
    ///
    /// - parameter message: The text to display
    /// - parameter size: The text size
    /// - parameter duration: How long to display the toast message for
    func toast(_ message:String, size:ToastSize = .normal, duration: ToastDuration = .normal) {
        let lbl = ToastLabel()
        lbl.textColor = UIColor.black
        lbl.backgroundColor = UIColor.darkGray
        lbl.text = "\n\(message)\n"
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.alpha = 0
        lbl.numberOfLines = 0
        for subView in subviews {
            if let existingToast = subView as? ToastLabel {
                existingToast.removeFromSuperview() // multiple overlapping toasts in the same place don't look good
            }
        }
        switch size {
        case .small:
            lbl.font = UIFont.systemFont(ofSize: 13)
        case .normal:
            // lbl.font = UIFont.init(name: Montserrat_Regular, size: 15)
           lbl.font = UIFont.systemFont(ofSize: 15)
        case .large:
           lbl.font = UIFont.systemFont(ofSize: 17)

        }
        
        lbl.clipsToBounds = true
        lbl.layer.cornerRadius = 15
        

        
        addSubview(lbl)
        addConstraints([
            NSLayoutConstraint(item: self, attribute: .leadingMargin, relatedBy: .equal, toItem: lbl, attribute: .leading, multiplier: 1, constant: -20),
            NSLayoutConstraint(item: self, attribute: .trailingMargin, relatedBy: .equal, toItem: lbl, attribute: .trailing, multiplier: 1, constant: 20),
            NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: lbl, attribute: .centerY, multiplier: 1, constant: 0),
            ])
        
        UIView.animate(withDuration: 0.1, animations: { lbl.alpha = 1.0 }, completion: { f in
            UIView.animate(withDuration: 1.2, delay: duration.rawValue, options:.curveEaseOut, animations: { lbl.alpha = 0 }, completion: { f in
                  lbl.text = ""
                lbl.removeFromSuperview() }) })
    }
}

/// Creates a toast message as a subview of the application's key window
///
/// - parameter message: The text to display
/// - parameter size: The text size
/// - parameter duration: How long to display the toast message for
func toast(_ message:String, size:ToastSize = .normal, duration: ToastDuration = .normal) {
    guard let window = UIApplication.shared.keyWindow else {
        return
    }
    window.toast(message, size: size, duration: duration)
}
