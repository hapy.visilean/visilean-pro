//
//  AppConstants.swift
//  Visilean Pro
//
//  Created by hapy gohil on 13/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//


import Foundation
import UIKit


struct Application {
    static let Delegate = UIApplication.shared.delegate as! AppDelegate
    
}

struct colors {
    static let  PrimaryBlue =  UIColor.init(colorCode: "1A2E5A")
    static let  MainBackground =  UIColor.init(colorCode: "F4F7FC")
    static let  colorTabNormal =  UIColor.init(colorCode: "C4C4C4")
    static let  colorTabSelectAll =  UIColor.init(colorCode: "02295B")
    static let  colorTabSelectMakeReady =  UIColor.init(colorCode: "FF7261")
    static let  colorTabSelectInProgress =  UIColor.init(colorCode: "541B60")
    static let  colorTabSelectRejected =  UIColor.init(colorCode: "5A55B5")
    static let  colorTabSelectQC =  UIColor.init(colorCode: "024009")
    static let  colorProjectLbl1 =  UIColor.init(colorCode: "038303")
    static let  colorProjectLbl2 =  UIColor.init(colorCode: "FFA500")
    static let  colorProjectLbl3 =  UIColor.init(colorCode: "FF0800")
    static let  colorProjectLbl4 =  UIColor.init(colorCode: "666666")
    static let  colorStartByNormal =  UIColor.init(colorCode: "919191")
    static let  colorStartByDateover =  UIColor.init(colorCode: "FF0800")

}

struct Defaults {
    static let shared =  UserDefaults.standard
}

struct DefaultsKeys {
    static let userId = "UserID"
    static let user = "User"
    static let token = UserDefaults.standard.object(forKey: "TOKEN") as? String ?? ""
}

struct Message {
    static let title = "Visilean Pro"
}

func getToken() -> String  {
    return Defaults.shared.object(forKey:DefaultsKeys.token) as? String ?? ""
}

func getUser() -> NSDictionary  {
    let dict = NSDictionary()
    if let data = Defaults.shared.object(forKey: DefaultsKeys.user) {
        let userDict:NSDictionary = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as! NSDictionary
        return userDict
    }
    return dict
}

var hasSafeArea: Bool {
    guard #available(iOS 11.0, *), let topPadding = UIApplication.shared.keyWindow?.safeAreaInsets.top, topPadding > 24 else {
        return false
    }
    return true
}
        
func dateToString(date:Date) -> String
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = "dd-MM-yyyy HH:mm"
    dateformatter.locale = Locale(identifier: "nl-NL")
    
    let create_d = dateformatter.string(from: date)
    return create_d
}

struct Device {
    // iDevice detection code
    static let IS_IPHONE = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad

    static let SCREEN_WIDTH = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )

    static let IS_IPHONE_5 = IS_IPHONE && SCREEN_MAX_LENGTH == 568 // 5, 5S, 5C, SE
    static let IS_IPHONE_6 = IS_IPHONE && SCREEN_MAX_LENGTH == 667 // 6, 6S, 7, 7S, 8, 8S
    static let IS_IPHONE_6P = IS_IPHONE && SCREEN_MAX_LENGTH == 736 // 6+, 6S+, 7+, 8+
    static let IS_IPHONE_X = IS_IPHONE && SCREEN_MAX_LENGTH == 812 // X, XS, 11 Pro
    static let IS_IPHONE_XS_MAX = IS_IPHONE && SCREEN_MAX_LENGTH == 896 // XR, XS Max, 11, 11 Pro Max
}

func checkDevice() -> Bool
{
    if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
        case 1136:
            print("iPhone 5 or 5S or 5C")
            
        case 1334:
            print("iPhone 6/6S/7/8")
            
        case 1920, 2208:
            print("iPhone 6+/6S+/7+/8+")
            
        case 2436:
            print("iPhone X, XS")
            return true
            
        case 2688:
            print("iPhone XS Max")
            return true
            
        case 1792:
            print("iPhone XR")
            return true
            
        default:
            print("Unknown")
        }
    }
    else if UIDevice().userInterfaceIdiom == .pad
    {
        switch UIScreen.main.nativeBounds.height {
           

        case 1024,1536:
            print("iPad 9.7 inch/Ratina")
            
        case 2048:
            print("iPad air 5th")
        case 2224:
            print("iPad air 3rd")
        case 1668:
            print("iPad air 11 inch")
            
        case 2732:
            print("iPad air 12.9 inch")
            return true
        default:
            print("Unknown")
             print(UIScreen.main.nativeBounds.height)
        }
    }
    return false
}
