//
//  UIStoryboard+Loader.swift
//  Visilean Pro
//
//  Created by hapy gohil on 13/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//


import UIKit

fileprivate enum Storyboard : String {
    case main = "Main"
    case standardUser = "StandardUser"
}

fileprivate extension UIStoryboard {
    
    static func loadFromMain(_ identifier: String) -> UIViewController {
        return load(from: .main, identifier: identifier)
    }
    // add convenience methods for other storyboards here ...
    
    // ... or use the main loading method directly when instantiating view controller
    // from a specific storyboard
    static func load(from storyboard: Storyboard, identifier: String) -> UIViewController {
        let uiStoryboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        return uiStoryboard.instantiateViewController(withIdentifier: identifier)
    }
    
    static func loadFromStandardUser(_ identifier: String) -> UIViewController {
        return load(from: .standardUser, identifier: identifier)
    }
}

// MARK: App View Controllers

extension UIStoryboard {
    
    // From Main storyboard
    
    static func ViewControllerScreen() -> ViewController {
        return loadFromMain("ViewController") as! ViewController
    }
    static func ListViewControllerScreen() -> ListViewController {
           return loadFromMain("ListViewController") as! ListViewController
    }
    
    static func IntroControllerScreen() -> IntroController {
           return loadFromMain("IntroController") as! IntroController
    }
    static func LoginPage() -> LoginController {
           return loadFromMain("LoginController") as! LoginController
    }
    
    static func RegisterStep1() -> RegisterStep1Controller {
           return loadFromMain("RegisterStep1Controller") as! RegisterStep1Controller
    }
    
    static func RegisterStep2() -> RegisterStep2Controller {
           return loadFromMain("RegisterStep2Controller") as! RegisterStep2Controller
    }
    
    static func RegisterStep3() -> RegisterStep3Controller {
           return loadFromMain("RegisterStep3Controller") as! RegisterStep3Controller
    }
    
    static func RegisterOTP() -> RegisterOTPController {
           return loadFromMain("RegisterOTPController") as! RegisterOTPController
    }
    
    static func ForgotPassword() -> ForgotPasswordController {
           return loadFromMain("ForgotPasswordController") as! ForgotPasswordController
    }
    
    static func ProjectList() -> ProjectListController {
              return loadFromMain("ProjectListController") as! ProjectListController
    }
    
    // From standard user storyboard
    
    static func standardUserTab() -> standardUserTabs {
        return loadFromStandardUser("standardUserTabs") as! standardUserTabs
    }
    
    static func leftMenu() -> LeftMenuViewController {
        return loadFromStandardUser("LeftMenuViewController") as! LeftMenuViewController
    }
    
    static func rightMenu() -> RightMenuViewController {
        return loadFromStandardUser("RightMenuViewController") as! RightMenuViewController
    }
    
    static func TaskList() -> TaskListViewController {
                return loadFromStandardUser("TaskListViewController") as! TaskListViewController
    }
}


