//
//  AlertManager.swift
//  Visilean Pro
//
//  Created by hapy gohil on 13/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//


import Foundation
import UIKit

protocol ShowsAlert {}

extension ShowsAlert where Self: UIViewController {
    func showAlert(message: String, handler: (() -> ())? = nil) {
        let strAppName = NSLocalizedString("Title", comment: "The name of the App")
        let alertController = UIAlertController(title: strAppName, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            if let handler = handler {
                handler()
            }
        }))
        present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(title : String ,message: String, handler: (() -> ())? = nil) {
        let strAppName = NSLocalizedString(title, comment: message)
        let alertController = UIAlertController(title: strAppName, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            if let handler = handler {
                handler()
            }
        }))
        present(alertController, animated: true, completion: nil)
    }
}

