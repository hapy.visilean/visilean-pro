//
//  IndicatorView.swift
//  Visilean Pro
//
//  Created by hapy gohil on 29/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import Foundation
import UIKit

public class IndicatorView {

    public static let sharedInstance = IndicatorView()
    var blurImg = UIImageView()
    var centerImage = UIImageView()
    var indicator = UIActivityIndicatorView()

    private init()
    {
        blurImg.frame = UIScreen.main.bounds
        blurImg.backgroundColor = UIColor.black
        blurImg.isUserInteractionEnabled = true
        blurImg.alpha = 0.5
        
        centerImage.frame = CGRect.init(x: (UIScreen.main.bounds.size.width / 2) - 50 , y: (UIScreen.main.bounds.size.height / 2) - 50, width: 100, height: 100)
        centerImage.backgroundColor = UIColor.clear
        let jeremyGif = UIImage.gifImageWithName("construction")
        centerImage.image = jeremyGif
        
        indicator.style = .whiteLarge
        indicator.center = blurImg.center
        indicator.startAnimating()
        indicator.color = .red
    }

    func showIndicator(){
        DispatchQueue.main.async( execute: {

            UIApplication.shared.keyWindow?.addSubview(self.blurImg)
            UIApplication.shared.keyWindow?.addSubview(self.centerImage)
        })
    }
    func hideIndicator(){

        DispatchQueue.main.async( execute:
            {
                self.blurImg.removeFromSuperview()
                self.indicator.removeFromSuperview()
                self.centerImage.removeFromSuperview()
        })
    }
}
