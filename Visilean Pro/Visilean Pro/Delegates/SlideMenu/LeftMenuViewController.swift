//
//  LeftMenuViewController.swift
//  TabbarWithSideMenu
//
//  Created by Sunil Prajapati on 20/04/18.
//  Copyright © 2018 sunil.prajapati. All rights reserved.
//

import UIKit
class LeftMenuViewController: UIViewController {

    //MARK:- IBOutlet And Variable Declaration
    var leftMenu = ["Tab View", "Main View", "Non Menu View", "Settings Tab", "Pin Tab"]
    var userNames = ["Hapy Gohil" , "Sharad Yadav" , "Viranj patel"]
    @IBOutlet weak var tableLeftMenu: UITableView!
    
    @IBOutlet var tvProfiles: UITableView!
    //MARK:- UIViewController Initialize Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableLeftMenu.tableFooterView = UIView()
        tvProfiles.dataSource = self
        tvProfiles.delegate = self
        
        tvProfiles.separatorColor = UIColor.clear
        tableLeftMenu.separatorColor = UIColor.clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
            if slideMenuController()?.isLeftOpen() == false
            {
                self.openMenuOptions()
            }
            
            
        }
        

        func openMenuOptions() {
            self.tableLeftMenu.frame = CGRect(x: 0 - 200, y: 0,width: self.view.frame.size.width - 120,height: self.view.frame.size.height)
            self.tvProfiles.frame = CGRect(x: 0, y: 0,width:  120,height: self.view.frame.size.height)
            
            startAnimating()
        }
        
        func startAnimating() {
            
            
            UIView.animateKeyframes(withDuration: 1, delay: 0,  animations: {
                
                if self.tableLeftMenu.frame.origin.x == 320
                {
                    //self.tableLeftMenu.center.x += self.view.bounds.width
                }
                else
                {
                    self.tableLeftMenu.center.x += 320
                }
                

            }, completion: nil)
        }
    //MARK:- Other Methods
    func changeMainViewController(to viewController: UIViewController) {
        //Change main viewcontroller of side menu view controller
        let navigationViewController = UINavigationController(rootViewController: viewController)
        slideMenuController()?.changeMainViewController(navigationViewController, close: true)
    }
}

//MARK:- UITableViewDataSource Methods
extension LeftMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tvProfiles
        {
            
            return userNames.count
        }
        
        return leftMenu.count
    }
     
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        if tableView == tvProfiles
        {
            let cell = tvProfiles.dequeueReusableCell(withIdentifier: "TableViewCell") as! TableViewCell
            
            cell.img_profile_pic.makeRoundedCornerView(Radius: cell.img_profile_pic.frame.size.width / 2)
            
            cell.lbl_firstname.text = userNames[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = tableLeftMenu.dequeueReusableCell(withIdentifier: "TableViewCell2") as! TableViewCell
        
        //cell.img_profile_pic.makeRoundedCornerView(Radius: cell.img_profile_pic.frame.size.width / 2)
        
        cell.lbl_firstname.text = leftMenu[indexPath.row]
        cell.selectionStyle = .none
        
        
        return cell
    }
}

//MARK:- UITableViewDelegate Methods
extension LeftMenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        guard let leftHeaderView = loadFromNibNamed(ViewID.leftHeaderView) as? LeftHeaderView else {
            print("Left Header view not found")
            return nil
        }
        
        //leftHeaderView.lblUserName.text = "Saruin"
        return leftHeaderView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if tableView == tvProfiles
        {
            return 0
        }
        
        return 160
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tvProfiles
        {
            return 140
        }
        else
        {
            return 44
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let leftMenuItem = LeftMenuItems(rawValue: leftMenu[indexPath.row]) else {
            print("Left Menu Item not found")
            return
        }
        
        switch leftMenuItem {
            
        case .tabView: break
                
        case .mainView: break
                
        case .nonMenu: break
                
        case .settingsTab, .pinTab:
               
                let toIndex = (leftMenuItem == .settingsTab) ? TabItem.settings.rawValue : TabItem.pin.rawValue
                
                //Check here Custom TabBar ViewController is already exist then we just set index other wise we instantiate view controller and set index
                
        }
    }
}
