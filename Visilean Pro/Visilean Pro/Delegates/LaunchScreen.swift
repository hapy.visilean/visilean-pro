//
//  LaunchScreen.swift
//  Visilean Pro
//
//  Created by Gopal on 20/05/20.
//  Copyright © 2020 Gopal. All rights reserved.
//

import UIKit

class LaunchScreen: UIViewController {
    var LaunchTimer: Timer?
    @IBOutlet weak var imgLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        LaunchTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: false)
        self.imgLogo.transform = CGAffineTransform.identity.scaledBy(x: 0, y: 0) // Scale your image
        UIView.animate(withDuration: 1.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                // HERE
            self.imgLogo.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1) // Scale your image

            }) { (finished) in
                 self.imgLogo.transform = CGAffineTransform.identity // undo in 1 seconds
        }
    }
    
    @objc func runTimedCode()
    {
        if UserDefaults.exists(key: "TOKEN") == true
        {
            if UserDefaults.exists(key: "projectGuid") == true
            {
                let projId = UserDefaults.standard.object(forKey: "projectGuid") as? String ?? ""
                if projId != ""
                {
                    self.loadStandardUserScreen()
                }
                else
                {
                    loadProjectListScreen()
                }
            }
            else
            {
                loadProjectListScreen()
            }
        }
        else
        {
            loadIntroScreen()
        }
         
    }
    
    func loadIntroScreen()
    {
        let VC = UIStoryboard.IntroControllerScreen()
        self.navigationController?.pushViewController(VC, animated: false)
    }
    
    func loadProjectListScreen()
    {
        Application.Delegate.makeRootProjectList()
    }
    
    func loadStandardUserScreen()
    {
        Application.Delegate.makeRootStandardUser()
    }

}
